/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.context;

import com.starventure.editor.iface.Drawable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Maks
 */
public class EditorContext {

    private static EditorContext context;
    private Set<Drawable> gameObjects = new HashSet<Drawable>();

    public boolean addObject(Drawable object) {
        return gameObjects.add(object);
    }

    public boolean removeObject(Drawable object) {
        return gameObjects.remove(object);
    }

    public void clearObjects() {
        gameObjects.clear();
    }

    public Set<Drawable> getAllObjects() {
        return Collections.unmodifiableSet(gameObjects);
    }

    public static EditorContext getContext() {
        if (context != null) {
            context = new EditorContext();
        }
        return context;
    }
}
