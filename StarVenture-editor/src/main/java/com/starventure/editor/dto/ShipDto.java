/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.dto;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Maks
 */
public class ShipDto implements Serializable {

    private String name;
    private List<Point> hitbox = new ArrayList<Point>();
    private Set<HardPointDto> hardpoints = new HashSet<HardPointDto>();
    private BufferedImage texture;
    private float maxHitPoints;
    private float turnSpeed;
    private float accel;
    private String spriteName;

    public ShipDto() {
    }

    public ShipDto(String name, List<Point> hitbox, Set<HardPointDto> hardPoints, BufferedImage texture) {
        this.name = name;
        this.hitbox = hitbox;
        this.hardpoints = hardPoints;
        this.texture = texture;
    }

    public List<Point> getHitbox() {
        return hitbox;
    }

    public void addHitboxPoint(Point hpPoint) {
        if (hitbox == null) {
            hitbox = new ArrayList<Point>();
        }
        hitbox.add(hpPoint);
    }

    public void setHitbox(List<Point> hitbox) {
        this.hitbox = hitbox;
    }

    public Set<HardPointDto> getHardpoints() {
        return Collections.unmodifiableSet(hardpoints);
    }

    public void addHardpoint(HardPointDto hardpoint) {
        if (hardpoints == null) {
            hardpoints = new HashSet<HardPointDto>();
        }
        hardpoints.add(hardpoint);
    }

    public void setHardpoints(Set<HardPointDto> hardpoints) {
        this.hardpoints = hardpoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BufferedImage getTexture() {
        return texture;
    }

    public void setTexture(BufferedImage texture) {
        this.texture = texture;
    }

    public float getMaxHitPoints() {
        return maxHitPoints;
    }

    public void setMaxHitPoints(float maxHitPoints) {
        this.maxHitPoints = maxHitPoints;
    }

    public float getTurnSpeed() {
        return turnSpeed;
    }

    public void setTurnSpeed(float turnSpeed) {
        this.turnSpeed = turnSpeed;
    }

    public float getAccel() {
        return accel;
    }

    public void setAccel(float accel) {
        this.accel = accel;
    }

    public String getSpriteName() {
        return spriteName;
    }

    public void setSpriteName(String spriteName) {
        this.spriteName = spriteName;
    }
}
