/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.dto;

import com.starventure.editor.iface.HardPoint;
import java.io.Serializable;

/**
 *
 * @author Maks
 */
public class HardPointDto implements HardPoint, Serializable {

    private int x;
    private int y;
    private float angle;
    private float face;

    @Override
    public int getX() {
        return x;
    }

    @Override
    public void setX(int x) {
        this.x = x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public float getAngle() {
        return angle;
    }

    @Override
    public void setAngle(float angle) {
        this.angle = angle;
    }

    @Override
    public float getFace() {
        return face;
    }

    @Override
    public void setFace(float face) {
        this.face = face;
    }
}
