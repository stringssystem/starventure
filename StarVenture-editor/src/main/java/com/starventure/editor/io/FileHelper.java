/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maks
 */
public class FileHelper {

    private static final Logger LOGGER = Logger.getLogger(FileHelper.class.getName());
    private static final String CHARSET_DEFAULT = "UTF-8";

    public static void savetoFile(File file, String string) {

        if (file == null) {
            throw new IllegalArgumentException("Argument file cannot be null");
        }

        OutputStreamWriter writer = null;

        try {
            writer = new OutputStreamWriter(new FileOutputStream(file), CHARSET_DEFAULT);
            writer.write(string);
            writer.flush();
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, "Can't open file " + file.getName(), ex);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.log(Level.SEVERE, CHARSET_DEFAULT + " encoding is not supported", ex);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Exception has occured while writing to file " + file.getName(), ex);
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException ex) {
                LOGGER.log(Level.WARNING, "Writer closing failed", ex);
            }
        }
    }
}
