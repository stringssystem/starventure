/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.validation;

import java.awt.Color;
import javax.swing.JTextField;

/**
 *
 * @author Maks
 */
public class Validator {

    public static boolean isParsableToFloat(String string) {
        try {
            Float.parseFloat(string);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isParsableToInt(String string) {
        try {
            Integer.parseInt(string);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isValueParsableToInt(JTextField field, Color errorColor) {
        boolean result = isParsableToInt(field.getText());
        if (!result) {
            field.setBackground(errorColor);
        }
        return result;
    }

    public static boolean isValueParsableToFloat(JTextField field, Color errorColor) {
        boolean result = isParsableToFloat(field.getText());
        if (!result) {
            field.setBackground(errorColor);
        }
        return result;
    }
    
    public static boolean valueNotNullOrEmpty(JTextField field, Color errorColor) {
        boolean result = (field.getText() != null && !field.getText().isEmpty());
        if (!result) {
            field.setBackground(errorColor);
        }
        return result;
    }
}
