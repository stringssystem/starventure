/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.pto;

import com.starventure.editor.dto.HardPointDto;
import com.starventure.editor.iface.HardPoint;
import static com.starventure.editor.visual.RenderConstants.*;
import com.starventure.editor.visual.utils.RenderUtils;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Maks
 */
public class HardPointPto extends BasicPto implements HardPoint {

    private HardPointDto hardPointDto;

    public HardPointPto() {
        hardPointDto = new HardPointDto();
    }

    public HardPointPto(HardPointDto hardPointDto) {
        this.hardPointDto = hardPointDto;
    }

    @Override
    public float getAngle() {
        return hardPointDto.getAngle();
    }

    @Override
    public float getFace() {
        return hardPointDto.getFace();
    }

    @Override
    public int getX() {
        return hardPointDto.getX();
    }

    @Override
    public int getY() {
        return hardPointDto.getY();
    }

    @Override
    public void setAngle(float angle) {
        hardPointDto.setAngle(angle);
    }

    @Override
    public void setFace(float face) {
        hardPointDto.setFace(face);
    }

    @Override
    public void setX(int x) {
        hardPointDto.setX(x);
    }

    @Override
    public void setY(int y) {
        hardPointDto.setY(y);
    }

    private AffineTransform constructTransformation() {
        AffineTransform aft = new AffineTransform();
        
        aft.rotate(getFace());
        aft.translate(getX(), getY());
        return aft;
    }

    public HardPointDto getHardPointDto() {
        return hardPointDto;
    }

    public void setHardPointDto(HardPointDto hardPointDto) {
        this.hardPointDto = hardPointDto;
    }

    
    
    @Override
    public void draw(Graphics2D g, AffineTransform parentTransform) {
        draw(g, parentTransform, COLOR_HARDPOINT);
    }

    @Override
    public void draw(Graphics2D g, AffineTransform parentTransform, Color c) {
        setColor(c, g);
        AffineTransform transform = constructTransformation();
        if (parentTransform != null) {
            transform.concatenate((AffineTransform)parentTransform.clone());
        }
       
        RenderUtils.fill(shape, transform, g);
    }
}
