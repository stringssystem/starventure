/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.pto;

import com.starventure.editor.iface.Translatable;
import static com.starventure.editor.visual.RenderConstants.*;
import com.starventure.editor.visual.utils.RenderUtils;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Maks
 */
public class HitboxPointPto extends BasicPto implements Translatable {

    private Point point = new Point();

    public HitboxPointPto() {
    }

    public HitboxPointPto(int x, int y) {
        point.x = x;
        point.y = y;
    }

    @Override
    public int getX() {
        return point.x;
    }

    @Override
    public void setX(int x) {
        point.x = x;
    }

    @Override
    public int getY() {
        return point.y;
    }

    @Override
    public void setY(int y) {
        point.y = y;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    
    
    private AffineTransform constructTransformation() {
        AffineTransform aft = new AffineTransform();
        
        aft.translate(getX(), getY());
        return aft;
    }

    @Override
    public void draw(Graphics2D g, AffineTransform parentTransform) {
        draw(g, parentTransform, COLOR_HITBOX_POINT);
    }

    @Override
    public void draw(Graphics2D g, AffineTransform parentTransform, Color c) {
        setColor(c, g);
        AffineTransform transform = constructTransformation();
        if (parentTransform != null) {
            transform.concatenate((AffineTransform)parentTransform.clone());
        }
        
        RenderUtils.fill(shape, transform, g);
    }
}
