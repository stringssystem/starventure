/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.pto;

import static com.starventure.editor.Constants.*;
import com.starventure.editor.iface.Drawable;
import com.starventure.editor.visual.utils.RenderUtils;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;

/**
 *
 * @author Maks
 */
public abstract class BasicPto implements Drawable {

    protected Shape shape;

    public BasicPto() {
        this(RenderUtils.generateBasicShape(SIZE_DEFAULT_SHAPE));
    }

    public BasicPto(Shape shape) {
        this.shape = shape;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    protected void setColor(Color c, Graphics2D g) {
        if (!g.getColor().equals(c)) {
            g.setColor(c);
        }
    }
}
