/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.filter;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Maks
 */
public class ImageFileFilter extends FileFilter {

    @Override
    public boolean accept(File f) {
        String name = f.getName().toLowerCase();
        return f.isDirectory() || name.endsWith(".bmp") || name.endsWith(".jpg") || name.endsWith(".png");
    }

    @Override
    public String getDescription() {
        return "Images (.bmp .png .jpg)";
    }
}
