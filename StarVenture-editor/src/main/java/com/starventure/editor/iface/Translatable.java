/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.iface;


/**
 *
 * @author Maks
 */
public interface Translatable{

    int getX();

    int getY();

    void setX(int x);

    void setY(int y);
}
