package com.starventure.editor.iface;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Maks
 */
public interface HardPoint extends Translatable {

    float getAngle();

    float getFace();

    void setAngle(float angle);

    void setFace(float face);
}
