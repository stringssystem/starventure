/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.iface;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;

/**
 *
 * @author Maks
 */
public interface Drawable {

    public void draw(Graphics2D g, AffineTransform parentTransform);

    public void draw(Graphics2D g, AffineTransform parentTransform, Color c);
}
