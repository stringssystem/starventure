/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor;

import java.awt.Color;

/**
 *
 * @author Maks
 */
public class Constants {

    public static final float ZOOM_MAX = 4f;
    public static final float ZOOM_MIN = 0.5f;
    public static final float ZOOM_STEP = 0.1f;
    public static final int SIZE_DEFAULT_SHAPE = 5;
    
    //FORM
    public static final Color COLOR_FIELD_DEFAULT = Color.WHITE;
    public static final Color COLOR_FIELD_ERROR = new Color(255, 128, 128);
}
