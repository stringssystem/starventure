/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.visual;

import com.starventure.editor.dto.HardPointDto;
import com.starventure.editor.dto.ShipDto;
import com.starventure.editor.pto.BasicPto;
import com.starventure.editor.pto.HardPointPto;
import com.starventure.editor.pto.HitboxPointPto;
import static com.starventure.editor.visual.RenderConstants.*;
import com.starventure.editor.visual.utils.RenderUtils;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.awt.RenderingHints;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Maks
 */
public class ShipTextureComponent extends AbstractShipTextureComponent {

    private static final String TEXT_NO_TEXTURE = "Please choose texture.";
    private ShipDto ship = new ShipDto();

    public void importShip(ShipDto ship) {
        this.ship = ship;

        List<Point> hitboxPoints = ship.getHitbox();
        Set<HardPointDto> hardPoints = ship.getHardpoints();

        if (hitboxPoints != null) {
            for (Point hitboxPoint : hitboxPoints) {
                hitboxShapes.add(new HitboxPointPto(hitboxPoint.x, hitboxPoint.y));
            }
        }
        if (hardPoints != null) {
            for (HardPointDto hardPointDto : hardPoints) {
                hardPointsShapes.add(new HardPointPto(hardPointDto));
            }
        }
        eventsDisabled = false;
        repaint();
    }
    
    public ShipDto exportShip(){
        List<Point> hitboxPoints = new ArrayList<Point>();
        Set<HardPointDto> hardPoints = new HashSet<HardPointDto>();
        
        for(HardPointPto pto : hardPointsShapes){
            hardPoints.add(pto.getHardPointDto());
        }
        for(HitboxPointPto pto : hitboxShapes){
            hitboxPoints.add(0, pto.getPoint());
        }
        ship.setHardpoints(hardPoints);
        ship.setHitbox(hitboxPoints);
        return ship;
    }

    public void changeTexture(BufferedImage image) {
        ship.setTexture(image);
        hardPointsShapes.clear();
        hitboxShapes.clear();
        eventsDisabled = false;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        g2d.scale(scale, scale);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        drawTexture(g2d);
        drawHitbox(g2d);
        drawHardPoints(g2d);

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        g2d.scale(1 / scale, 1 / scale);
        drawGraphics(g2d);
    }

    private void drawHardPoints(Graphics2D g) {
        for (BasicPto hardpointShape : hardPointsShapes) {
            drawGameObject(hardpointShape, g);
        }
    }

    private void drawHitbox(Graphics2D g) {
        g.setColor(COLOR_HITBOX_POINT);

        Polygon envelope = RenderUtils.getEnvelope(hitboxShapes);

        RenderUtils.draw(envelope, screenTransform, g);

        for (BasicPto hitboxShape : hitboxShapes) {
            drawGameObject(hitboxShape, g);
        }
    }

    private void drawGameObject(BasicPto object, Graphics2D g) {
        //object.setScale(scale);
        if (object.equals(focusedGameObject)) {
            object.draw(g, screenTransform, COLOR_FOCUSED_OBJECT);
        } else {
            object.draw(g, screenTransform);
        }
    }

    private void drawTexture(Graphics2D g) {
        BufferedImage texture = null;

        if (ship != null) {
            texture = ship.getTexture();
        }

        if (texture == null) {
            int height = this.getHeight();
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());
            g.setColor(Color.WHITE);
            g.drawString(TEXT_NO_TEXTURE, 5, height - 5);
        } else {
            AffineTransform textureTransform = new AffineTransform();
            textureTransform.translate(-(texture.getWidth() / 2), -(texture.getHeight() / 2));
            textureTransform.concatenate(screenTransform);

            g.drawImage(texture, textureTransform, null);
        }
    }

    private void drawGraphics(Graphics2D g) {
        if (ship.getTexture() != null) {
            g.setColor(COLOR_TEXT);
            g.setXORMode(COLOR_TEXT_XOR);
            g.drawString("Zoom: " + Math.round(scale * 100) + "%", 5, getHeight() - 5);
        }
    }
}
