/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.visual;

import com.starventure.editor.Constants;
import java.awt.Color;

/**
 *
 * @author Maks
 */
public class RenderConstants extends Constants{
    public static final Color COLOR_HITBOX_POINT = Color.RED;
    public static final Color COLOR_HARDPOINT = Color.YELLOW;
    public static final Color COLOR_FOCUSED_OBJECT = Color.GREEN;
    public static final Color COLOR_TEXT = Color.BLACK;
    public static final Color COLOR_TEXT_XOR = Color.WHITE;
}
