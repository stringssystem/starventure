/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.visual.utils;

import com.starventure.editor.iface.Translatable;
import com.starventure.editor.pto.HitboxPointPto;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.util.Collection;

/**
 *
 * @author Maks
 */
public class RenderUtils {

    public static Polygon generateBasicShape(int size) {
        int[] xPoints = new int[]{0, (-size * 2) / 3, -size, (-size * 2) / 3, 0, (size * 2) / 3, size, (size * 2) / 3};
        int[] yPoints = new int[]{size, (size * 2) / 3, 0, (-size * 2) / 3, -size, (-size * 2) / 3, 0, (size * 2) / 3};

        return new Polygon(xPoints, yPoints, xPoints.length);
    }

    public static Polygon getEnvelope(Collection<HitboxPointPto> hitbox) {
        int size = hitbox.size();
        int[] xPoints = new int[size];
        int[] yPoints = new int[size];

        int index = 0;
        for (Translatable point : hitbox) {
            xPoints[index] = point.getX();
            yPoints[index] = point.getY();
            index++;
        }
        return new Polygon(xPoints, yPoints, size);
    }
    
    public static void draw(Shape object, AffineTransform transform, Graphics2D g){
        AffineTransform defaultTransformation = g.getTransform();
        transformGraphics(transform, g);
        g.draw(object);
        g.setTransform(defaultTransformation);
    }
    
    public static void fill(Shape object, AffineTransform transform, Graphics2D g){
        AffineTransform defaultTransformation = g.getTransform();
        transformGraphics(transform, g);
        g.fill(object);
        g.setTransform(defaultTransformation);
    }
    
    /**
     * Adds transformation to graphics object
     * @param transform that should be added to graphics object
     * @param g graphics object
     * @return the default transformation
     */
    private static void transformGraphics(AffineTransform transform, Graphics2D g){
        
        AffineTransform objectTransformation = (AffineTransform) g.getTransform().clone();
        objectTransformation.concatenate(transform);
        g.setTransform(objectTransformation);
    }
}
