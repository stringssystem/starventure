/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.starventure.editor.visual;

import static com.starventure.editor.Constants.*;
import com.starventure.editor.iface.Translatable;
import com.starventure.editor.pto.HardPointPto;
import com.starventure.editor.pto.HitboxPointPto;
import java.awt.Point;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.JComponent;

/**
 *
 * @author Maks
 */
public abstract class AbstractShipTextureComponent extends JComponent {

    protected Set<HardPointPto> hardPointsShapes = new HashSet<HardPointPto>();
    protected List<HitboxPointPto> hitboxShapes = new ArrayList<HitboxPointPto>();
    protected Point center = new Point();
    protected AffineTransform screenTransform = new AffineTransform();
    protected Translatable focusedGameObject;
    protected boolean eventsDisabled = true;
    protected float scale = 1f;

    public AbstractShipTextureComponent() {
        attachMouseListener();
        attachMouseMotionListener();
        attachWindowListener();
        attachMouseWheelListener();
        computeScreenCenterAndTransformation();
    }

    private void computeScreenCenterAndTransformation() {
        center = new Point(
                (int) (getWidth() / (2 * scale)),
                (int) (getHeight() / (2 * scale)));

        screenTransform = new AffineTransform();
        screenTransform.translate(center.x, center.y);
    }

    private void attachWindowListener() {
        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                //central point location has been changed
                computeScreenCenterAndTransformation();
                super.componentResized(e);
            }
        });
    }

    private void attachMouseListener() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (eventsDisabled) {
                    return;
                }

                Point cursorLocation = translateCursorLocation(e);

                focusedGameObject = getGamePointAt(cursorLocation.x, cursorLocation.y);
                if (focusedGameObject == null) {
                    focusedGameObject = createPoint(cursorLocation.x, cursorLocation.y, e);
                } else {
                    if (e.getClickCount() > 1) {
                        //Trust me NetBeans! I am an engineer! :-D
                        if (focusedGameObject instanceof HardPointPto) {
                            hardPointsShapes.remove(focusedGameObject);
                        } else {
                            hitboxShapes.remove(focusedGameObject);
                        }
                    }
                }
                repaint();
                super.mousePressed(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (eventsDisabled) {
                    return;
                }

                focusedGameObject = null;
                repaint();
                super.mouseReleased(e);
            }
        });
    }

    private void attachMouseMotionListener() {
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {
                if (eventsDisabled) {
                    return;
                }

                Point cursorLocation = translateCursorLocation(e);
                if (focusedGameObject != null) {
                    focusedGameObject.setX(cursorLocation.x);
                    focusedGameObject.setY(cursorLocation.y);
                    repaint();
                }
                super.mouseDragged(e); //To change body of generated methods, choose Tools | Templates.
            }
        });
    }

    private void attachMouseWheelListener() {
        this.addMouseWheelListener(new MouseWheelListener() {
            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (e.isControlDown()) {
                    int rot = e.getWheelRotation();
                    if (rot < 0 && scale < (ZOOM_MAX - (ZOOM_STEP / 2))) {
                        scale += ZOOM_STEP;
                    } else {
                        if (rot > 0 && scale > (ZOOM_MIN + (ZOOM_STEP / 2))) {
                            scale -= ZOOM_STEP;
                        }
                    }
                }
                computeScreenCenterAndTransformation();
                repaint();
            }
        });
    }

    private Translatable createPoint(int x, int y, MouseEvent e) {
        Translatable createdPoint;
        if (e.getButton() == MouseEvent.BUTTON1) {
            HitboxPointPto hitboxPoint = new HitboxPointPto();
            hitboxPoint.setX(x);
            hitboxPoint.setY(y);
            hitboxShapes.add(hitboxPoint);
            createdPoint = hitboxPoint;
        } else {
            HardPointPto hardPoint = new HardPointPto();
            hardPoint.setX(x);
            hardPoint.setY(y);
            hardPointsShapes.add(hardPoint);
            createdPoint = hardPoint;
        }
        return createdPoint;
    }

    private Translatable getGamePointAt(int x, int y) {
        for (HardPointPto hp : hardPointsShapes) {
            if (hp.getShape().contains(x - hp.getX(), y - hp.getY())) {
                return hp;
            }
        }
        for (HitboxPointPto hp : hitboxShapes) {
            if (hp.getShape().contains(x - hp.getX(), y - hp.getY())) {
                return hp;
            }
        }
        return null;
    }

    private Point translateCursorLocation(MouseEvent e) {
        return new Point(
                (int) ((e.getX() / scale - center.x)),
                (int) ((e.getY() / scale - center.y)));
    }
}
