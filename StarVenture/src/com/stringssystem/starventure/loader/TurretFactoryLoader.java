package com.stringssystem.starventure.loader;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.stringssystem.starventure.ship.component.TurretFactory;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 06.08.13
 * Time: 0:11
 * To change this template use File | Settings | File Templates.
 */
public class TurretFactoryLoader extends AsynchronousAssetLoader<TurretFactory, TurretFactoryLoader.TurretLoaderParameters> {

    private TurretFactory data;

    public TurretFactoryLoader(FileHandleResolver filehandler) {
        super(filehandler);
    }

    @Override
    public void loadAsync(AssetManager assetManager, String s, TurretLoaderParameters weaponLoaderParameters) {
        Json json = new Json();
        data = json.fromJson(TurretFactory.class, resolve(s));
    }

    @Override
    public TurretFactory loadSync(AssetManager assetManager, String s, TurretLoaderParameters weaponLoaderParameters) {
        return data;
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String s, TurretLoaderParameters weaponLoaderParameters) {
        Array<AssetDescriptor> deps = new Array<AssetDescriptor>();
        deps.add(new AssetDescriptor("data/spritepack/pack.atlas", TextureAtlas.class));
        return deps;
    }

    static class TurretLoaderParameters extends AssetLoaderParameters<TurretFactory> {
    }
}
