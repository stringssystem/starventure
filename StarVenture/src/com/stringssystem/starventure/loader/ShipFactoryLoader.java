package com.stringssystem.starventure.loader;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.stringssystem.starventure.ship.ShipFactory;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 03.08.13
 * Time: 20:22
 * To change this template use File | Settings | File Templates.
 */
public class ShipFactoryLoader extends AsynchronousAssetLoader<ShipFactory, ShipFactoryLoader.ShipLoaderParameters> {

    private ShipFactory data;

    public ShipFactoryLoader(FileHandleResolver filehandler) {
        super(filehandler);
    }

    @Override
    public Array<AssetDescriptor> getDependencies(String s, ShipFactoryLoader.ShipLoaderParameters aVoid) {
        Array<AssetDescriptor> deps = new Array<AssetDescriptor>();
        deps.add(new AssetDescriptor("data/spritepack/pack.atlas", TextureAtlas.class));
        return deps;
    }

    @Override
    public void loadAsync(AssetManager assetManager, String s, ShipFactoryLoader.ShipLoaderParameters aVoid) {
        Json js = new Json();
        data = js.fromJson(ShipFactory.class, resolve(s));
    }

    @Override
    public ShipFactory loadSync(AssetManager assetManager, String s, ShipFactoryLoader.ShipLoaderParameters aVoid) {
        return data;  //To change body of implemented methods use File | Settings | File Templates.
    }

    static class ShipLoaderParameters extends AssetLoaderParameters<ShipFactory> {
    }
}
