package com.stringssystem.starventure.background;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.Resources;

public class Debris extends Sprite {

    private static final int BUFFER = 100;
    private float SPEED = 5.0f;
    private float LIFETIME = MathUtils.random(8, 12);
    private float FADE_TIME = 2;

    private float random_direction = MathUtils.random(-360, 360);
    private float random_scale = MathUtils.random() * 0.75f + 0.5f;
    private float random_speed = (MathUtils.random() * 2f) - 1f;
    private float random_opacity = MathUtils.random() * 0.35f + 0.6f;

    private Vector2 position = new Vector2();
    private Vector2 facing = new Vector2(1, 0);

    public boolean alive = true;

    private float since_alive = 0;

    public Debris(Vector2 position) {
        super();
        this.position = position;
        this.setPosition(position.x, position.y);

        this.facing.rotate(random_direction);
        this.setScale(random_scale, random_scale);
        int imgRand = MathUtils.random(0, 1);
        switch (MathUtils.random(0, 2)) {
            case 0:
                this.set(Resources.getInstance().getSprite("debris_sml" + imgRand));
                break;
            case 1:
                this.set(Resources.getInstance().getSprite("debris_med" + imgRand));
                break;
            default:
                this.set(Resources.getInstance().getSprite("debris_lrg" + imgRand));
                break;
        }
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        super.draw(spriteBatch);

        float delta = Math.min(0.06f, Gdx.graphics.getDeltaTime());

        since_alive += delta;

        facing.rotate((SPEED + random_speed) * delta).nor();
        position.add(facing.mul((SPEED + random_speed) * delta));
        this.setPosition(position.x, position.y);

        if (since_alive < FADE_TIME) {
            super.setColor(1, 1, 1, Math.min((since_alive / FADE_TIME) * random_opacity, random_opacity));
        } else {
            this.setColor(1, 1, 1, Math.min(1 - (since_alive - LIFETIME + FADE_TIME) / FADE_TIME, 1) * random_opacity);
        }
        if (since_alive > LIFETIME) {
            alive = false;
            this.setColor(1, 1, 1, 0);
        }
    }

    public void reset(float x, float y, float width, float height) {
        SPEED = 5.0f;
        LIFETIME = MathUtils.random(8, 12);
        FADE_TIME = 2;

        random_direction = MathUtils.random(-360, 360);
        random_scale = MathUtils.random() * 0.75f + 0.5f;
        random_speed = (MathUtils.random() * 2f) - 1f;
        random_opacity = MathUtils.random() * 0.35f + 0.6f;

        alive = true;
        since_alive = 0;

        this.position.set(
                MathUtils.random(x - (width / 2) - BUFFER, x + (width / 2) + BUFFER),
                MathUtils.random(y - (height / 2) - BUFFER, y + (height / 2) + BUFFER));
        facing.set(1, 0);

        this.setPosition(position.x, position.y);

        this.facing.rotate(random_direction);
        this.setScale(random_scale, random_scale);
    }
}
