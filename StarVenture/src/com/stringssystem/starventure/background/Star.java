package com.stringssystem.starventure.background;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.Resources;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 04.09.13
 * Time: 10:46
 * To change this template use File | Settings | File Templates.
 */
public class Star extends Sprite {

    private static final int BUFFER = 100;
    private float LIFETIME = 5;
    private float FADE_TIME = 2;

    private float random_direction = MathUtils.random(-360, 360);
    private float random_scale = MathUtils.random() * 0.75f + 0.5f;
    private float random_opacity = MathUtils.random() * 0.35f + 0.6f;

    private Vector2 position = new Vector2();
    private Vector2 facing = new Vector2(1, 0);

    public boolean alive = true;

    private float since_alive = 0;

    public Star(Vector2 position) {
        super();
        this.position = position;
        this.setPosition(position.x, position.y);

        this.facing.rotate(random_direction);
        this.setScale(random_scale, random_scale);
        int imgRand = MathUtils.random(0, 3);
        this.set(Resources.getInstance().getSprite("star" + imgRand));
    }

    @Override
    public void draw(SpriteBatch spriteBatch) {
        super.draw(spriteBatch);

        float delta = Math.min(0.06f, Gdx.graphics.getDeltaTime());

        since_alive += delta;
        this.setPosition(position.x, position.y);

        if (since_alive < FADE_TIME) {
            super.setColor(1, 1, 1, Math.min((since_alive / FADE_TIME) * random_opacity, random_opacity));
        } else {
            this.setColor(1, 1, 1, Math.min(1 - (since_alive - LIFETIME + FADE_TIME) / FADE_TIME, 1) * random_opacity);
        }
        if (since_alive > LIFETIME) {
            alive = false;
            this.setColor(1, 1, 1, 0);
        }
    }

    public void reset(float x, float y, float width, float height) {
        LIFETIME = MathUtils.random(8, 12);
        FADE_TIME = 2;

        random_direction = MathUtils.random(-360, 360);
        random_scale = MathUtils.random() * 0.75f + 0.5f;
        random_opacity = MathUtils.random() * 0.35f + 0.6f;

        alive = true;
        since_alive = 0;

        this.position.set(
                MathUtils.random(x - (width / 2) - BUFFER, x + (width / 2) + BUFFER),
                MathUtils.random(y - (height / 2) - BUFFER, y + (height / 2) + BUFFER));
        facing.set(1, 0);

        this.setPosition(position.x, position.y);

        this.facing.rotate(random_direction);
        this.setScale(random_scale, random_scale);
    }
}
