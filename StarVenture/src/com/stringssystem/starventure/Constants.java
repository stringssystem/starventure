package com.stringssystem.starventure;

public final class Constants {

    public static final String SETTINGS_KEY = "starventure_settings";
    public static final boolean DEBUG = false;
    public static final float WORLD_TO_BOX = 0.01f;
    public static final float BOX_TO_WORLD = 100f;
    public static final String[] SUPPORTED_RESOLUTIONS = new String[]{"1024x768", "1280x1024", "1366x768", "1920x1080"};
}
