package com.stringssystem.starventure;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Disposable;
import com.stringssystem.starventure.loader.ParticleEffectLoader;
import com.stringssystem.starventure.loader.ShipFactoryLoader;
import com.stringssystem.starventure.loader.TurretFactoryLoader;
import com.stringssystem.starventure.postprocess.ShaderLoader;
import com.stringssystem.starventure.ship.ShipFactory;
import com.stringssystem.starventure.ship.component.TurretFactory;

public class Resources implements Disposable {

    private AssetManager manager = new AssetManager();

    public void init() {
        manager.setLoader(ShipFactory.class, new ShipFactoryLoader(new InternalFileHandleResolver()));
        manager.setLoader(TurretFactory.class, new TurretFactoryLoader(new InternalFileHandleResolver()));
        manager.setLoader(ParticleEffect.class, new ParticleEffectLoader(new InternalFileHandleResolver()));
        manager.load("data/spritepack/pack.atlas", TextureAtlas.class);
        ShaderLoader.BasePath = "data/shaders/";
        loadAllMusic();
        loadAllSounds();
        loadAllWeapons();
        loadAllShips();
        loadAllParticles();
    }

    public Sprite getSprite(String spriteName) {
        if (manager.isLoaded("data/spritepack/pack.atlas")) {
            TextureAtlas atlas = manager.get("data/spritepack/pack.atlas");
            return atlas.createSprite(spriteName);
        }
        return null;
    }

    public TextureRegion getTexture(String spriteName) {
        if (manager.isLoaded("data/spritepack/pack.atlas")) {
            TextureAtlas atlas = manager.get("data/spritepack/pack.atlas");
            return atlas.findRegion(spriteName);
        }
        return null;
    }

    public Music getMusic(String name) {
        if (manager.isLoaded("data/audio/" + name + ".ogg")) {
            return manager.get("data/audio/" + name + ".ogg", Music.class);
        }
        return null;
    }

    public Sound getSound(String name) {
        if (manager.isLoaded("data/sound/" + name + ".wav")) {
            return manager.get("data/sound/" + name + ".wav", Sound.class);
        }
        return null;
    }

    public ParticleEffect getEffect(String name) {
        if (manager.isLoaded("data/particles/" + name + ".p")) {
            return manager.get("data/particles/" + name + ".p", ParticleEffect.class);
        }
        return null;
    }

    private void loadAllShips() {
        if (Gdx.files.internal("data/ships/").isDirectory()) {
            for (FileHandle handle : Gdx.files.internal("data/ships/").list("ship")) {
                manager.load(handle.path(), ShipFactory.class);
            }
        }
    }

    private void loadAllParticles() {
        if (Gdx.files.internal("data/particles/").isDirectory()) {
            for (FileHandle handle : Gdx.files.internal("data/particles/").list("p")) {
                manager.load(handle.path(), ParticleEffect.class);
            }
        }
    }

    private void loadAllWeapons() {
        if (Gdx.files.internal("data/weapons/").isDirectory()) {
            for (FileHandle handle : Gdx.files.internal("data/weapons/").list("wpn")) {
                manager.load(handle.path(), TurretFactory.class);
            }
        }
    }

    private void loadAllSounds() {
        if (Gdx.files.internal("data/sounds/").isDirectory()) {
            for (FileHandle handle : Gdx.files.internal("data/sounds/").list("wav")) {
                manager.load(handle.path(), Sound.class);
            }
        }
    }

    private void loadAllMusic() {
        if (Gdx.files.internal("data/audio/").isDirectory()) {
            for (FileHandle handle : Gdx.files.internal("data/audio/").list("ogg")) {
                manager.load(handle.path(), Music.class);
            }
        }
    }

    private static Resources instance;

    public static Resources getInstance() {
        if (instance == null) {
            instance = new Resources();
        }
        return instance;
    }

    public static AssetManager getManager() {
        return getInstance().manager;
    }

    public void dispose() {
        manager.dispose();
    }

    public Texture getRandomBackground() {
        int rand = MathUtils.random(1, 3);
        return new Texture(Gdx.files.internal("data/background/background" + rand + ".jpg"));
    }
}
