package com.stringssystem.starventure.ship;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.stringssystem.starventure.api.*;
import com.stringssystem.starventure.module.ParticleEffectsModule;
import com.stringssystem.starventure.module.ModuleManager;
import com.stringssystem.starventure.object.GameObjectImpl;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 13.07.13
 * Time: 14:10
 * To change this template use File | Settings | File Templates.
 */
public class Ship extends GameObjectImpl implements ContainsRenderables, Disposable {


    private ShipState state;
    public final EngineSystemsControl engineSystemsControl = new EngineSystemsControl(this);
    public final ShipHull shipHull = new ShipHull(this);
    public final CombatSystemsControl combatSystemsControl = new CombatSystemsControl(this);
    private final Array<Renderable> drawables = new Array<Renderable>();
    private Renderable shipRenderable;
    private boolean enemy;
    private ShipAI ai;

    public boolean isEnemy() {
        return enemy;
    }

    public void setEnemy(boolean enemy) {
        this.enemy = enemy;
        for (Fixture fixture : this.getBody().getFixtureList()) {
            if (fixture.getUserData().equals(FixtureType.HULL) || fixture.getUserData().equals(FixtureType.WRAPPER)) {
                Filter filterData = fixture.getFilterData();
                if (!enemy) {
                    filterData.categoryBits = GameObjectCategory.FRIENDLY_SHIP.mask();
                    filterData.maskBits = GameObjectMask.ENEMY.mask();
                } else {
                    filterData.categoryBits = GameObjectCategory.ENEMY_SHIP.mask();
                    filterData.maskBits = GameObjectMask.FRIENDLY.mask();
                }
                fixture.setFilterData(filterData);
                break;
            }
        }
    }

    public Ship() {
        state = ShipState.NONE;
    }

    public ShipState getState() {
        return state;
    }

    public void setState(ShipState state) {
        this.state = state;
    }


    @Override
    public void update(float delta) {
        combatSystemsControl.update();
        engineSystemsControl.update();
        shipHull.update();
        super.update(delta);
        shipRenderable.getPosition().set(getWorldPosition());
        shipRenderable.setRotation(getRotationInDegrees());
    }

    public void damage(float amount) {
        shipHull.setHitPoints(Math.max(shipHull.getHitPoints() - amount, 0));
        if (shipHull.getHitPoints() <= 0) {
            destruct();
        }
    }

    void destruct() {
        ModuleManager.getModule(ParticleEffectsModule.class).explode(this);
        setAlive(false);
    }


    @Override
    protected Body makeBody(BodyDef.BodyType bodyType, Vector2 pos, float angle) {
        Body body = super.makeBody(bodyType, pos, angle);
        body.setAngularDamping(0.5f);
        body.setLinearDamping(0.5f);
        body.setBullet(true);
        return body;
    }

    public void setDrawable(Renderable renderable) {
        this.shipRenderable = renderable;
        this.drawables.add(renderable);
    }

    @Override
    public Array<Renderable> getRenderables() {
        for (ShipComponent comp : combatSystemsControl.getComponents()) {
            if (comp instanceof ContainsRenderables) {
                for (Renderable draw : ((ContainsRenderables) comp).getRenderables()) {
                    if (!drawables.contains(draw, true)) {
                        drawables.add(draw);
                    }
                }
            }
        }
        return drawables;
    }

    @Override
    public void spawn(Vector2 position, float facing) {
        super.spawn(position, facing);
        setEnemy(false);
    }

    @Override
    public void dispose() {
        engineSystemsControl.dispose();
        combatSystemsControl.dispose();
    }

    public enum ShipState {THRUSTING_FORWARD, BOOSTING_FORWARD, TURNING_LEFT, BREAKING, TURNING_RIGHT, NONE}
}
