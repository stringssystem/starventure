package com.stringssystem.starventure.ship.hardpoint;

import com.stringssystem.starventure.ship.component.ComponentType;
import com.stringssystem.starventure.ship.component.Engine;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 25.09.13
 * Time: 22:36
 * To change this template use File | Settings | File Templates.
 */
public class EngineHardpoint extends AbstractHardpoint<Engine> {

    private float nozzleDirection;

    public float getNozzleDirection() {
        return nozzleDirection;
    }

    public void setNozzleDirection(float firingArc) {
        this.nozzleDirection = firingArc;
    }

    @Override
    public ComponentType getType() {
        return ComponentType.ENGINE;
    }
}
