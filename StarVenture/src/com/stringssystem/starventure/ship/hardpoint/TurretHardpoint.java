package com.stringssystem.starventure.ship.hardpoint;

import com.stringssystem.starventure.ship.component.ComponentType;
import com.stringssystem.starventure.ship.component.Turret;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 01.08.13
 * Time: 9:18
 * To change this template use File | Settings | File Templates.
 */
public class TurretHardpoint extends AbstractHardpoint<Turret> {

    private float firingArc = 5;
    private float initialAngle;

    public float getFiringArc() {
        return firingArc;
    }

    public void setFiringArc(float firingArc) {
        this.firingArc = firingArc;
    }

    @Override
    public ComponentType getType() {
        return ComponentType.TURRET;
    }

    public float getInitialAngle() {
        return initialAngle;
    }

    public void setInitialAngle(float initialAngle) {
        this.initialAngle = initialAngle;
    }
}
