package com.stringssystem.starventure.ship.hardpoint;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.api.Hardpoint;
import com.stringssystem.starventure.api.ShipComponent;
import com.stringssystem.starventure.ship.Ship;
import com.stringssystem.starventure.ship.component.ComponentType;
import com.stringssystem.starventure.util.Box2dUtil;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 25.09.13
 * Time: 22:18
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractHardpoint<T extends ShipComponent> implements Hardpoint<T> {
    private Vector2 initialPosition;
    private Ship ship;
    private final Vector2 boxPosition = new Vector2();
    private final Vector2 worldPosition = new Vector2();
    private static final Vector2 tmp = new Vector2();
    private T mountedEquipment;

    @Override
    public abstract ComponentType getType();

    @Override
    public void setInitialPosition(Vector2 initialPosition) {
        this.initialPosition = initialPosition;
    }

    public Vector2 getInitialPosition() {
        return initialPosition;
    }

    @Override
    public void setShip(Ship ship) {
        this.ship = ship;
    }

    @Override
    public Ship getShip() {
        return ship;
    }

    public T getMountedEquipment() {
        return mountedEquipment;
    }

    public void setMountedEquipment(T mountedEquipment) {
        if (this.getType() == mountedEquipment.getType()) {
            this.mountedEquipment = mountedEquipment;
            this.mountedEquipment.setHardpoint(this);
        }
    }

    @Override
    public Vector2 getBoxPosition() {
        tmp.set(Box2dUtil.ConvertToBox(initialPosition.x), Box2dUtil.ConvertToBox(initialPosition.y));
        Vector2 pos = tmp.rotate(ship.getBody().getAngle() * MathUtils.radiansToDegrees);
        Vector2 shipPos = ship.getBody().getPosition();
        return boxPosition.set(pos.x + shipPos.x, pos.y + shipPos.y);
    }

    @Override
    public Vector2 getWorldPosition() {
        Vector2 tmp = getBoxPosition();
        return worldPosition.set(Box2dUtil.ConvertToWorld(tmp.x), Box2dUtil.ConvertToWorld(tmp.y));
    }
}
