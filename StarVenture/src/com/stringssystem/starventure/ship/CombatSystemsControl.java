package com.stringssystem.starventure.ship;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.stringssystem.starventure.api.ShipComponent;
import com.stringssystem.starventure.ship.hardpoint.TurretHardpoint;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 01.09.13
 * Time: 10:10
 * To change this template use File | Settings | File Templates.
 */
public class CombatSystemsControl implements Disposable {

    private Ship ship;
    private Array<TurretHardpoint> hardpoints = new Array<TurretHardpoint>();
    private Array<ShipComponent> componentArray = new Array<ShipComponent>();

    public CombatSystemsControl(Ship ship) {
        this.ship = ship;
    }

    public void addHardpoint(TurretHardpoint hp) {
        hp.setShip(ship);
        hardpoints.add(hp);
    }

    public void addHardpoints(Array<TurretHardpoint> hps) {
        for (TurretHardpoint hp : hps) {
            hp.setShip(ship);
            hardpoints.add(hp);
        }
    }

    public Array<TurretHardpoint> getHardpoints() {
        return hardpoints;
    }

    public void update() {
        for (ShipComponent comp : getComponents()) {
            comp.update();
        }
    }

    public <T> Array<T> getComponents(Class<T> clazz) {
        Array<T> retArray = new Array<T>();
        for (TurretHardpoint hp : hardpoints) {
            if (hp.getMountedEquipment() != null && hp.getMountedEquipment().getClass().isAssignableFrom(clazz))
                retArray.add((T) hp.getMountedEquipment());
        }
        return retArray;
    }

    public Array<ShipComponent> getComponents() {
        componentArray.clear();
        for (TurretHardpoint hp : hardpoints) {
            if (hp.getMountedEquipment() != null)
                componentArray.add(hp.getMountedEquipment());
        }
        return componentArray;
    }

    @Override
    public void dispose() {
        for (ShipComponent comp : getComponents()) {
            comp.dispose();
        }
    }
}
