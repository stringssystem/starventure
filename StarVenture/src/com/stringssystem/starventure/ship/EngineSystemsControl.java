package com.stringssystem.starventure.ship;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Pool;
import com.stringssystem.starventure.api.Hardpoint;
import com.stringssystem.starventure.api.ShipComponent;
import com.stringssystem.starventure.ship.hardpoint.EngineHardpoint;
import com.stringssystem.starventure.util.Box2dUtil;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 01.09.13
 * Time: 10:09
 * To change this template use File | Settings | File Templates.
 */
public class EngineSystemsControl implements Disposable {

    private Ship ship;
    private float turnSpeed = 0;
    private float acceleration = 0;
    private Array<EngineHardpoint> hardpoints = new Array<EngineHardpoint>();
    private Pool<Vector2> vectPool = new Pool<Vector2>() {
        @Override
        protected Vector2 newObject() {
            return new Vector2();
        }
    };
    private Array<ShipComponent> componentArray = new Array<ShipComponent>();

    public EngineSystemsControl(Ship ship) {
        this.ship = ship;
    }

    public void addHardpoint(EngineHardpoint hp) {
        hp.setShip(ship);
        hardpoints.add(hp);
    }

    public void addHardpoints(Array<EngineHardpoint> hps) {
        for (EngineHardpoint hp : hps) {
            hp.setShip(ship);
            hardpoints.add(hp);
        }
    }

    public Array<EngineHardpoint> getHardpoints() {
        return hardpoints;
    }

    public void turn(float direction) {
        ship.getBody().applyTorque(direction * Box2dUtil.ConvertToBox(turnSpeed));
    }

    public void thrust() {
        thrust(1);
    }

    public void backwardThrust() {
        thrust(-1f);
    }

    public float getTurnSpeed() {
        return turnSpeed;
    }

    public void setTurnSpeed(float turnSpeed) {
        this.turnSpeed = turnSpeed;
    }

    public float getAcceleration() {
        return acceleration;
    }

    public void setAcceleration(float acceleration) {
        this.acceleration = acceleration;
    }

    void thrust(float amount) {
        Vector2 vect = vectPool.obtain();
        ship.getBody().applyForce(
                vect.set(
                        ship.getRotationInVector().x * ship.getBody().getMass() * amount,
                        ship.getRotationInVector().y * ship.getBody().getMass() * amount),
                ship.getBoxPosition());
        vectPool.free(vect);
        getHardpoints().get(0).getMountedEquipment().activate();
        getHardpoints().get(1).getMountedEquipment().activate();
    }

    void goTowardsOrAway(Vector2 targetPos, boolean forceThrust, boolean isAway) {
        Vector2 targetDirection = targetPos.tmp().sub(ship.getWorldPosition());
        if (isAway) {
            targetDirection.mul(-1);
        }

        float nextAngle = ship.getBody().getAngle() + (ship.getBody().getAngularVelocity() / 3);
        float totalRotation = (targetDirection.angle() * MathUtils.degreesToRadians) - nextAngle;
        while (totalRotation < (-180 * MathUtils.degreesToRadians)) {
            totalRotation += (360 * MathUtils.degreesToRadians);
        }
        while (totalRotation > (180 * MathUtils.degreesToRadians)) {
            totalRotation -= (360 * MathUtils.degreesToRadians);
        }
        if (totalRotation > 0) {
            turn(1);
        } else {
            turn(-1);
        }

        Vector2 nextStep = ship.getBody().getPosition().cpy().add((ship.getBody().getLinearVelocity().x / 3), (ship.getBody().getLinearVelocity().y / 3));
        Vector2 totalStep = targetPos.cpy().sub(Box2dUtil.ConvertToWorld(nextStep));
        if (forceThrust || totalStep.len() > 30) {
            thrust(Box2dUtil.ConvertToBox(totalStep.len() / 10));
        } else {
            thrust(Box2dUtil.ConvertToBox(-totalStep.len() / 10));
        }
    }

    // automatically thrusts and turns according to the target
    public void goTowards(Vector2 targetPos, boolean forceThrust) {
        goTowardsOrAway(targetPos, forceThrust, false);
    }

    public void goAway(Vector2 targetPos, boolean forceThrust) {
        goTowardsOrAway(targetPos, forceThrust, true);
    }

    public <T> Array<T> getComponents(Class<T> clazz) {
        Array<T> retArray = new Array<T>();
        for (Hardpoint hp : hardpoints) {
            if (hp.getMountedEquipment() != null && hp.getMountedEquipment().getClass().isAssignableFrom(clazz))
                retArray.add((T) hp.getMountedEquipment());
        }
        return retArray;
    }

    public Array<ShipComponent> getComponents() {
        componentArray.clear();
        for (Hardpoint hp : hardpoints) {
            if (hp.getMountedEquipment() != null)
                componentArray.add(hp.getMountedEquipment());
        }
        return componentArray;
    }

    public void update() {
        for (ShipComponent comp : getComponents()) {
            comp.update();
        }
    }

    public void stop() {
        getHardpoints().get(0).getMountedEquipment().deactivate();
        getHardpoints().get(1).getMountedEquipment().deactivate();
    }

    @Override
    public void dispose() {
        for (ShipComponent comp : getComponents()) {
            comp.dispose();
        }
    }
}
