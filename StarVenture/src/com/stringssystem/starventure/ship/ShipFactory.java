package com.stringssystem.starventure.ship;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;
import com.stringssystem.starventure.api.Factory;
import com.stringssystem.starventure.api.FixtureType;
import com.stringssystem.starventure.api.Layer;
import com.stringssystem.starventure.object.TextureRegionRenderable;
import com.stringssystem.starventure.ship.hardpoint.EngineHardpoint;
import com.stringssystem.starventure.ship.hardpoint.TurretHardpoint;
import com.stringssystem.starventure.util.Box2dUtil;
import com.stringssystem.starventure.util.JSONUtil;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 05.08.13
 * Time: 23:48
 * To change this template use File | Settings | File Templates.
 */
public class ShipFactory implements Factory<Ship>, Json.Serializable {

    private String spriteName = "";
    private String name = "";
    private float turnSpeed = 0f;
    private float maxHitPoints = 0f;
    private float acceleration = 0f;
    private Array<TurretHardpoint> turretHardpoints = new Array<TurretHardpoint>();
    private Array<EngineHardpoint> engineHardpoints = new Array<EngineHardpoint>();
    private Array<Vector2> hullPolygon = new Array<Vector2>();
    private Array<Vector2> wrapperPolygon = new Array<Vector2>();
    private Array<Vector2> detailPolygon = new Array<Vector2>();

    @Override
    public void write(Json json) {
        json.writeValue("name", name);
        json.writeValue("maxHitPoints", maxHitPoints);
        json.writeValue("spriteName", spriteName);
        json.writeValue("turnSpeed", turnSpeed);
        json.writeValue("accel", acceleration);
        json.addClassTag("turretHardpoints", TurretHardpoint.class);
        json.addClassTag("engineHardpoint", EngineHardpoint.class);
        json.addClassTag("point", Vector2.class);
        json.writeValue("turretHardpoints", turretHardpoints);
        json.writeValue("engineHardpoints", engineHardpoints);
        json.writeValue("hullPolygon", hullPolygon);
        json.writeValue("wrapperPolygon", wrapperPolygon);
        json.writeValue("detailPolygon", detailPolygon);
    }

    @Override
    public void read(Json json, OrderedMap<String, Object> stringObjectOrderedMap) {
        name = json.readValue("name", String.class, stringObjectOrderedMap);
        maxHitPoints = json.readValue("maxHitPoints", Float.class, stringObjectOrderedMap);
        spriteName = json.readValue("spriteName", String.class, stringObjectOrderedMap);
        turnSpeed = json.readValue("turnSpeed", Float.class, stringObjectOrderedMap);
        acceleration = json.readValue("accel", Float.class, stringObjectOrderedMap);
        Object tmp = stringObjectOrderedMap.get("turretHardpoints");
        if (tmp != null) {
            for (OrderedMap<String, Object> val : (Array<OrderedMap<String, Object>>) tmp) {
                turretHardpoints.add(retrieveTurretHardpoint(val));
            }
        }
        tmp = stringObjectOrderedMap.get("engineHardpoints");
        if (tmp != null) {
            for (OrderedMap<String, Object> val : (Array<OrderedMap<String, Object>>) tmp) {
                engineHardpoints.add(retrieveEngineHardpoint(val));
            }
        }
        tmp = stringObjectOrderedMap.get("wrapperPolygon");
        if (tmp != null) {
            for (OrderedMap<String, Object> vect : (Array<OrderedMap<String, Object>>) tmp) {
                wrapperPolygon.add(JSONUtil.readVector2(vect));
            }
        }
        tmp = stringObjectOrderedMap.get("detailPolygon");
        if (tmp != null) {
            for (OrderedMap<String, Object> vect : (Array<OrderedMap<String, Object>>) tmp) {
                detailPolygon.add(JSONUtil.readVector2(vect));
            }
        }
        tmp = stringObjectOrderedMap.get("hullPolygon");
        if (tmp != null) {
            for (OrderedMap<String, Object> vect : (Array<OrderedMap<String, Object>>) tmp) {
                hullPolygon.add(JSONUtil.readVector2(vect));
            }
        }
    }

    private TurretHardpoint retrieveTurretHardpoint(OrderedMap<String, Object> val) {
        TurretHardpoint hpnt = new TurretHardpoint();
        hpnt.setFiringArc((Float) val.get("firingArc", 10f));
        hpnt.setInitialAngle((Float) val.get("initialAngle", 0f));
        hpnt.setInitialPosition(JSONUtil.readVector2("position", val));
        return hpnt;
    }

    private EngineHardpoint retrieveEngineHardpoint(OrderedMap<String, Object> val) {
        EngineHardpoint hpnt = new EngineHardpoint();
        hpnt.setNozzleDirection((Float) val.get("nozzleDirection", -90f));
        hpnt.setInitialPosition(JSONUtil.readVector2("position", val));
        return hpnt;
    }

    @Override
    public Ship newInstance() {
        Ship newShip = new Ship();
        newShip.setDrawable(new TextureRegionRenderable(spriteName, Layer.SHIPS));
        newShip.shipHull.setName(name);
        newShip.shipHull.setMaxHitPoints(maxHitPoints);
        newShip.shipHull.setHitPoints(maxHitPoints);
        newShip.engineSystemsControl.setTurnSpeed(turnSpeed);
        newShip.engineSystemsControl.setAcceleration(acceleration);
        newShip.engineSystemsControl.addHardpoints(copyEngineHardpointArray(engineHardpoints));
        newShip.combatSystemsControl.addHardpoints(copyTurretHardpointArray(turretHardpoints));
        if (wrapperPolygon != null && wrapperPolygon.size > 0) {
            PolygonShape polygon = new PolygonShape();
            polygon.set(getVector2s(wrapperPolygon));
            FixtureDef fixtureDef = newShip.addFixtureDef(polygon, 0, FixtureType.WRAPPER);
            fixtureDef.isSensor = true;
        }
        if (hullPolygon != null && hullPolygon.size > 0) {
            PolygonShape polygon = new PolygonShape();
            polygon.set(getVector2s(hullPolygon));
            newShip.addFixtureDef(polygon, 1, FixtureType.HULL);
        }
        if (detailPolygon != null && detailPolygon.size > 0) {
            newShip.shipHull.setCollisionPoints(detailPolygon);
        }
        return newShip;
    }

    private Vector2[] getVector2s(Array<Vector2> polygon) {
        Array<Vector2> tmp = copyVector2Array(polygon);
        Vector2[] vector2s = new Vector2[tmp.size];
        int i = 0;
        for (Vector2 pnt : tmp) {
            vector2s[i++] = Box2dUtil.ConvertToBox(pnt);
        }
        return vector2s;
    }

    private Array<EngineHardpoint> copyEngineHardpointArray(Array<EngineHardpoint> array) {
        Array<EngineHardpoint> retVal = new Array<EngineHardpoint>(array.size);
        for (EngineHardpoint vec : array) {
            EngineHardpoint hpnt = new EngineHardpoint();
            hpnt.setNozzleDirection(vec.getNozzleDirection());
            hpnt.setInitialPosition(vec.getInitialPosition());
            retVal.add(hpnt);
        }
        return retVal;
    }

    private Array<TurretHardpoint> copyTurretHardpointArray(Array<TurretHardpoint> array) {
        Array<TurretHardpoint> retVal = new Array<TurretHardpoint>(array.size);
        for (TurretHardpoint vec : array) {
            TurretHardpoint hpnt = new TurretHardpoint();
            hpnt.setInitialAngle(vec.getInitialAngle());
            hpnt.setFiringArc(vec.getFiringArc());
            hpnt.setInitialPosition(vec.getInitialPosition());
            retVal.add(hpnt);
        }
        return retVal;
    }

    private Array<Vector2> copyVector2Array(Array<Vector2> array) {
        Array<Vector2> retVal = new Array<Vector2>(array.size);
        for (Vector2 vec : array) {
            retVal.add(vec.cpy());
        }
        return retVal;
    }

}
