package com.stringssystem.starventure.ship;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.stringssystem.starventure.util.Box2dUtil;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 01.09.13
 * Time: 10:09
 * To change this template use File | Settings | File Templates.
 */
public class ShipHull {

    private Ship ship;
    private float hitPoints = 0;
    private float maxHitPoints = 0;
    private String name = "";
    private Array<Vector2> collisionPoints = new Array<Vector2>();
    private Array<Vector2> translatedBoxPolygon = new Array<Vector2>();
    private Array<Vector2> translatedWorldPolygon = new Array<Vector2>();
    private Pool<Vector2> vector2Pool = new Pool<Vector2>() {
        @Override
        protected Vector2 newObject() {
            return new Vector2();
        }
    };
    private static final Vector2 tmp = new Vector2();

    public ShipHull(Ship ship) {
        this.ship = ship;
    }

    public float getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(float hitPoints) {
        this.hitPoints = hitPoints;
    }

    public float getMaxHitPoints() {
        return maxHitPoints;
    }

    public void setMaxHitPoints(float maxHitPoints) {
        this.maxHitPoints = maxHitPoints;
    }


    public float healthPercentage() {
        return Math.max(hitPoints / maxHitPoints, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCollisionPoints(Array<Vector2> collisionPoints) {
        this.collisionPoints.addAll(collisionPoints);
    }

    public Array<Vector2> getBoxCollisionPolygon() {
        return translatedBoxPolygon;
    }

    public Array<Vector2> getWorldCollisionPolygon() {
        return translatedWorldPolygon;
    }

    private Vector2 translate(Vector2 point) {
        tmp.set(Box2dUtil.ConvertToBox(point.x), Box2dUtil.ConvertToBox(point.y));
        Vector2 pos = tmp.rotate(ship.getBody().getAngle() * MathUtils.radiansToDegrees);
        Vector2 shipPos = ship.getBody().getPosition();
        return vector2Pool.obtain().set(pos.x + shipPos.x, pos.y + shipPos.y);
    }

    public void update() {
        vector2Pool.freeAll(translatedBoxPolygon);
        vector2Pool.freeAll(translatedWorldPolygon);
        translatedBoxPolygon.clear();
        translatedWorldPolygon.clear();
        for (Vector2 point : collisionPoints) {
            translatedBoxPolygon.add(translate(point));
            translatedWorldPolygon.add(Box2dUtil.ConvertToWorld(translate(point)));
        }
    }
}
