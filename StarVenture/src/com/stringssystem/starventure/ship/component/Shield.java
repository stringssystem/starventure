package com.stringssystem.starventure.ship.component;

import com.badlogic.gdx.physics.box2d.Fixture;
import com.stringssystem.starventure.api.FixtureBindable;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 02.09.13
 * Time: 8:37
 * To change this template use File | Settings | File Templates.
 */
public class Shield extends AbstractShipComponent implements FixtureBindable {

    private Fixture shieldFixture;

    @Override
    protected void doUpdate() {
        shieldFixture.setSensor(!isActive());
    }

    @Override
    protected void init() {
    }

    @Override
    public ComponentType getType() {
        return ComponentType.SHIELD;
    }

    @Override
    public void dispose() {
    }

    @Override
    public Fixture getFixture() {
        return shieldFixture;
    }

    @Override
    public void setFixture(Fixture fixture) {
        shieldFixture = fixture;
    }
}
