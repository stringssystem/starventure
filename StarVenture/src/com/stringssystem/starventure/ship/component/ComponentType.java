package com.stringssystem.starventure.ship.component;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 27.08.13
 * Time: 10:20
 * To change this template use File | Settings | File Templates.
 */
public enum ComponentType {
    HARDPOINT,
    TURRET,
    LAUNCHER,
    ENGINE,
    WEAPON,
    SHIELD
}
