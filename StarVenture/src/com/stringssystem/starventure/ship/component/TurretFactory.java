package com.stringssystem.starventure.ship.component;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;
import com.stringssystem.starventure.api.Factory;
import com.stringssystem.starventure.api.Layer;
import com.stringssystem.starventure.object.TextureRegionRenderable;
import com.stringssystem.starventure.projectile.ProjectileFactory;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 06.08.13
 * Time: 0:11
 * To change this template use File | Settings | File Templates.
 */
public class TurretFactory implements Factory<Turret>, Json.Serializable {

    private String name;
    private float magazineSize;
    private float shotReloadRate;
    private float shotCooldownTime;
    private ProjectileFactory weaponProjectile;
    private String spriteName;
    private float turnRate;

    @Override
    public void write(Json json) {
        json.writeValue("name", name);
        json.writeValue("magazineSize", magazineSize);
        json.writeValue("shotReloadRate", shotReloadRate);
        json.writeValue("weaponProjectile", weaponProjectile);
        json.writeValue("shotCooldownTime", shotCooldownTime);
        json.writeValue("spriteName", spriteName);
        json.writeValue("turnRate", turnRate);
    }

    @Override
    public void read(Json json, OrderedMap<String, Object> stringObjectOrderedMap) {
        name = json.readValue("name", String.class, stringObjectOrderedMap);
        magazineSize = json.readValue("magazineSize", Float.class, stringObjectOrderedMap);
        shotReloadRate = json.readValue("shotReloadRate", Float.class, stringObjectOrderedMap);
        weaponProjectile = json.readValue("weaponProjectile", ProjectileFactory.class, stringObjectOrderedMap);
        shotCooldownTime = json.readValue("shotCooldownTime", Float.class, stringObjectOrderedMap);
        turnRate = json.readValue("turnRate", Float.class, stringObjectOrderedMap);
        spriteName = json.readValue("spriteName", String.class, stringObjectOrderedMap);
    }

    @Override
    public Turret newInstance() {
        Turret newWeapon = new Turret();
        newWeapon.setName(name);
        newWeapon.setMagazineSize(magazineSize);
        newWeapon.setMagazineState(magazineSize);
        newWeapon.setShotReloadRate(shotReloadRate);
        newWeapon.setShotCooldownTime(shotCooldownTime);
        newWeapon.setProjectile(weaponProjectile);
        newWeapon.setTurnSpeed(turnRate);
        newWeapon.setRenderable(new TextureRegionRenderable(spriteName, Layer.MODULES));
        return newWeapon;
    }
}
