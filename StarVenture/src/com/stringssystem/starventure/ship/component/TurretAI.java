package com.stringssystem.starventure.ship.component;

import com.stringssystem.starventure.Targeting;
import com.stringssystem.starventure.api.AI;
import com.stringssystem.starventure.api.GameObject;
import com.stringssystem.starventure.api.Hardpoint;
import com.stringssystem.starventure.api.Projectile;
import com.stringssystem.starventure.ship.hardpoint.TurretHardpoint;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 03.09.13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class TurretAI implements AI {

    private final Turret weapon;
    private GameObject target;
    private Hardpoint hardpoint;
    private Projectile projectileSample;

    public TurretAI(Turret weapon) {
        this.weapon = weapon;
        hardpoint = weapon.getHardpoint();
        projectileSample = weapon.getProjectile().newInstance();
    }

    @Override
    public void retarget() {
        target = Targeting.getTargetInRange(hardpoint.getWorldPosition(), projectileSample.getRange(), weapon.getHardpoint(TurretHardpoint.class).getFiringArc());
    }

    @Override
    public void update() {
        retarget();
        if (targeted()) {
            float targetAngle = target.getWorldPosition().tmp().sub(hardpoint.getWorldPosition()).angle();
            weapon.setDesiredAngle(targetAngle);
            if (weapon.isReadyToShoot() && hasChanceToHit(targetAngle))
                weapon.shoot();
        } else {
            weapon.onDefaultFacing();
        }
    }

    private boolean targeted() {
        return target != null;
    }

    private boolean hasChanceToHit(float targetAngle) {
        float totalRotation = weapon.getFacing() - targetAngle;
        while (totalRotation < (-180)) {
            totalRotation += (360);
        }
        while (totalRotation > (180)) {
            totalRotation -= (360);
        }
        return Math.abs(totalRotation) < 10;
    }
}
