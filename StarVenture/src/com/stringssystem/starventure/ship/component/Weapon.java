package com.stringssystem.starventure.ship.component;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.stringssystem.starventure.api.ContainsRenderables;
import com.stringssystem.starventure.api.Renderable;
import com.stringssystem.starventure.projectile.ProjectileFactory;
import com.stringssystem.starventure.ship.hardpoint.TurretHardpoint;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 05.07.13
 * Time: 14:01
 * To change this template use File | Settings | File Templates.
 */
public class Weapon extends AbstractShipComponent implements ContainsRenderables {

    protected float shotCooldownTime = 0f;
    protected float magazineSize = 0f;
    protected float shotReloadRate = 0f;
    protected float magazineState = magazineSize;
    protected float cooldown = 0;
    protected ProjectileFactory weaponProjectile;
    protected Array<Renderable> drawables = new Array<Renderable>(1);
    protected float facing;

    public float getMagazineState() {
        return magazineState;
    }

    public void setMagazineState(float magazineState) {
        this.magazineState = magazineState;
    }

    public float getMagazineSize() {
        return magazineSize;
    }

    public void setMagazineSize(float magazineSize) {
        this.magazineSize = magazineSize;
    }

    public float getShotReloadRate() {
        return shotReloadRate;
    }

    public void setShotReloadRate(float shotReloadRate) {
        this.shotReloadRate = shotReloadRate;
    }

    public float getShotCooldownTime() {
        return shotCooldownTime;
    }

    public void setShotCooldownTime(float shotCooldownTime) {
        this.shotCooldownTime = shotCooldownTime;
    }

    public ProjectileFactory getProjectile() {
        return weaponProjectile;
    }

    public void setProjectile(ProjectileFactory weaponProjectile) {
        this.weaponProjectile = weaponProjectile;
    }

    boolean isEmpty() {
        return magazineState < 1;
    }

    boolean isCooledDown() {
        return cooldown == 0;
    }

    public boolean isReadyToShoot() {
        return isCooledDown() && !isEmpty();
    }

    public int getShotsLeft() {
        return (int) magazineState;
    }

    @Override
    protected void doUpdate() {
        float delta = Math.min(0.06f, Gdx.graphics.getDeltaTime());
        cooldown = Math.max(0, cooldown - delta * 50f);
        magazineState = Math.min(magazineState + (shotReloadRate * delta), magazineSize);
        drawables.get(0).getPosition().set(getHardpoint().getWorldPosition());
        facing = getShip().getRotationInDegrees() + getHardpoint(TurretHardpoint.class).getInitialAngle();
        drawables.get(0).setRotation(facing);
        if (isActive())
            shoot(getHardpoint().getWorldPosition(), facing);
    }

    @Override
    protected void init() {
    }

    @Override
    public ComponentType getType() {
        return ComponentType.WEAPON;
    }

    @Override
    public void dispose() {
        weaponProjectile = null;
    }

    public void shoot(Vector2 from, float inDirection) {
        if (cooldown == 0 && magazineState >= 1) {
            magazineState -= 1;
            cooldown = shotCooldownTime;
            weaponProjectile.newInstance().spawn(from, inDirection);
        }
    }

    public void shoot() {
        shoot(getHardpoint().getWorldPosition(), facing);
    }

    @Override
    public Array<Renderable> getRenderables() {
        return drawables;
    }

    public void setRenderable(Renderable renderable) {
        drawables.clear();
        drawables.add(renderable);
    }

    public float getFacing() {
        return facing;
    }
}
