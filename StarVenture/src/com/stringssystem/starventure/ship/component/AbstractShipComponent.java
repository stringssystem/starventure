package com.stringssystem.starventure.ship.component;

import com.stringssystem.starventure.api.Hardpoint;
import com.stringssystem.starventure.api.ShipComponent;
import com.stringssystem.starventure.ship.Ship;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 26.08.13
 * Time: 18:09
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractShipComponent implements ShipComponent {

    private Ship ship;
    private boolean active;
    private String name;
    private Hardpoint hardpoint;
    private boolean needToInit = true;

    @Override
    public Hardpoint getHardpoint() {
        return hardpoint;
    }

    @Override
    public <T extends Hardpoint> T getHardpoint(Class<T> clazz) {

        if (clazz.isAssignableFrom(hardpoint.getClass())) {
            return (T) hardpoint;
        }
        throw new IllegalArgumentException("Component not mounted on hardpoint of type: " + clazz.getName());
    }

    @Override
    public void setHardpoint(Hardpoint hardpoint) {
        this.hardpoint = hardpoint;
        this.ship = hardpoint.getShip();
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void activate() {
        active = true;
    }

    @Override
    public void deactivate() {
        active = false;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public Ship getShip() {
        return ship;
    }

    @Override
    public final void update() {
        if (needToInit) {
            init();
            needToInit = false;
        }
        doUpdate();
    }

    protected abstract void doUpdate();

    protected abstract void init();

}
