package com.stringssystem.starventure.ship.component;

import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.module.ParticleEffectsModule;
import com.stringssystem.starventure.module.ModuleManager;
import com.stringssystem.starventure.ship.hardpoint.EngineHardpoint;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 01.10.13
 * Time: 18:31
 * To change this template use File | Settings | File Templates.
 */
public class Engine extends AbstractShipComponent {

    ParticleEffectPool.PooledEffect effect;
    private String effectName = "";

    public String getEffectName() {
        return effectName;
    }

    public void setEffectName(String effectName) {
        this.effectName = effectName;
    }

    @Override
    public void doUpdate() {
        if (isActive()) {
            Vector2 pos = getHardpoint(EngineHardpoint.class).getWorldPosition();
            effect.setPosition(pos.x, pos.y);
            ParticleEmitter emitter = effect.getEmitters().first();
            float baseAngle = getShip().getRotationInDegrees() + getHardpoint(EngineHardpoint.class).getNozzleDirection();
            emitter.getAngle().setLow(baseAngle - 5);
            emitter.getAngle().setHigh(baseAngle + 5);
            emitter.start();
        } else {
            ParticleEmitter emitter = effect.getEmitters().first();
            if (!emitter.isComplete())
                emitter.allowCompletion();
        }
    }

    @Override
    protected void init() {
        effect = ModuleManager.getModule(ParticleEffectsModule.class).getEffect(effectName);
        ParticleEmitter emitter = effect.getEmitters().first();
        emitter.getAngle().setRelative(false);
        emitter.getScale().setHigh(20f);
        emitter.setAttached(true);
    }

    @Override
    public ComponentType getType() {
        return ComponentType.ENGINE;
    }

    @Override
    public void dispose() {
        ModuleManager.getModule(ParticleEffectsModule.class).freeEffect(effect);
    }
}
