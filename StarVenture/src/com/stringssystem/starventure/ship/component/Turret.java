package com.stringssystem.starventure.ship.component;

import com.badlogic.gdx.Gdx;
import com.stringssystem.starventure.ship.hardpoint.TurretHardpoint;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 04.09.13
 * Time: 17:58
 * To change this template use File | Settings | File Templates.
 */
public class Turret extends Weapon {

    private TurretAI ai;
    private float desiredAngle = 0;
    private float turnSpeed = 20;
    private float delta;

    public float getTurnSpeed() {
        return turnSpeed;
    }

    public void setTurnSpeed(float turnSpeed) {
        this.turnSpeed = turnSpeed;
    }

    public TurretAI getAi() {
        return ai;
    }

    public void setAi(TurretAI ai) {
        this.ai = ai;
    }

    public float getDesiredAngle() {

        return desiredAngle;
    }

    public void setDesiredAngle(float desiredAngle) {
        this.desiredAngle = desiredAngle;
    }

    public void turn(float direction) {
        facing += direction * delta * turnSpeed;
    }

    @Override
    protected void init() {
        facing = getShip().getRotationInDegrees() + getHardpoint(TurretHardpoint.class).getInitialAngle();
        ai = new TurretAI(this);
    }

    @Override
    public void doUpdate() {
        delta = Math.min(0.06f, Gdx.graphics.getDeltaTime());
        facing += getShip().getBody().getAngularVelocity();
        cooldown = Math.max(0, cooldown - delta * 50f);
        magazineState = Math.min(magazineState + (shotReloadRate * delta), magazineSize);
        drawables.get(0).getPosition().set(getHardpoint().getWorldPosition().x, getHardpoint().getWorldPosition().y);
        drawables.get(0).setRotation(facing);
        turnOnDesiredAngle();
        if (isActive())
            ai.update();
        else
            onDefaultFacing();
    }

    @Override
    public ComponentType getType() {
        return ComponentType.TURRET;
    }

    private void turnOnDesiredAngle() {
        float totalRotation = desiredAngle - facing;
        while (totalRotation < (-180)) {
            totalRotation += (360);
        }
        while (totalRotation > (180)) {
            totalRotation -= (360);
        }
        if (Math.abs(totalRotation) > 1) {
            if (totalRotation > 0) {
                turn(1);
            } else {
                turn(-1);
            }
        }
    }

    public void onDefaultFacing() {
        desiredAngle = getShip().getRotationInDegrees() + getHardpoint(TurretHardpoint.class).getInitialAngle();
    }
}
