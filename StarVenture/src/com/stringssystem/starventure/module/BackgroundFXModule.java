package com.stringssystem.starventure.module;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.stringssystem.starventure.api.Renderer;
import com.stringssystem.starventure.background.Asteroid;
import com.stringssystem.starventure.background.Debris;
import com.stringssystem.starventure.background.Star;
import com.stringssystem.starventure.renderer.GameCamera;

public final class BackgroundFXModule extends AbstractGameModule implements Renderer {

    private static final Vector2 middle = new Vector2();
    private final Array<Debris> debrises = new Array<Debris>();
    private final Array<Asteroid> asteroids = new Array<Asteroid>();
    private final Array<Star> stars = new Array<Star>();


    public BackgroundFXModule() {
        createDebris();
        //createAsteroids();
        createStars();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        createDebris();
        //createAsteroids();
        createStars();
    }

    private void createDebris() {
        debrises.clear();
        for (int i = 0; i < 30; ++i) {
            debrises.add(new Debris(new Vector2(MathUtils.random(-100, 800), MathUtils.random(-200, 400))));
        }
    }

    private void createAsteroids() {
        asteroids.clear();
        for (int i = 0; i < 15; ++i) {
            asteroids.add(new Asteroid(new Vector2(MathUtils.random(-100, 800), MathUtils.random(-200, 400))));
        }
    }

    private void createStars() {
        stars.clear();
        for (int i = 0; i < 15; ++i) {
            stars.add(new Star(new Vector2(MathUtils.random(-100, 800), MathUtils.random(-200, 400))));
        }
    }

    @Override
    public void render() {
        GameCamera.getInstance().screenToWorldCoords(
                middle.set(GameCamera.getInstance().getCamera().viewportWidth / 2,
                        GameCamera.getInstance().getCamera().viewportHeight / 2));
        for (Debris debris : debrises) {
            if (debris.alive) {
                debris.draw(mainBatch);
            } else {
                debris.reset(middle.x, middle.y, width, height);
            }
        }
        for (Asteroid asteroid : asteroids) {
            if (asteroid.alive) {
                asteroid.draw(mainBatch);
            } else {
                asteroid.reset();
            }
        }

        for (Star star : stars) {
            if (star.alive) {
                star.draw(mainBatch);
            } else {
                star.reset(middle.x, middle.y, width, height);
            }
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        asteroids.clear();
        debrises.clear();
        stars.clear();
    }

    @Override
    public void render(float delta) {
        render();
    }

    @Override
    public int getPosition() {
        return 10;
    }
}
