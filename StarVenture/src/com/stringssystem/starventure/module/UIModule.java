package com.stringssystem.starventure.module;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.stringssystem.starventure.Player;
import com.stringssystem.starventure.Resources;
import com.stringssystem.starventure.api.Layer;
import com.stringssystem.starventure.api.Renderable;
import com.stringssystem.starventure.controls.GameControlsMultiplexer;
import com.stringssystem.starventure.controls.GestureInputProcessor;
import com.stringssystem.starventure.controls.KeysInputProcessor;
import com.stringssystem.starventure.object.AnimationRenderable;
import com.stringssystem.starventure.ship.Ship;
import com.stringssystem.starventure.ship.component.Turret;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 30.08.13
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 */
public final class UIModule extends AbstractGameModule {

    private Skin skin;
    private Table main;
    private boolean loaded = false;
    private GestureInputProcessor gestures;
    private Renderable targetPointRenderable =
            new AnimationRenderable(0.175f, Layer.BACKGROUND, "target-1", "target-2", "target-3", "target-4");

    @Override
    public void init(SpriteBatch mainBatch, Stage stage) {
        super.init(mainBatch, stage);
        gestures = new GestureInputProcessor();
        GameControlsMultiplexer.getInstance().getMultiplexer().addProcessor(0, stage);
        GameControlsMultiplexer.getInstance().getMultiplexer().addProcessor(1, new GestureDetector(gestures));
        GameControlsMultiplexer.getInstance().getMultiplexer().addProcessor(2, new KeysInputProcessor());
        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        main = new Table();
        main.setFillParent(true);
        stage.addActor(main);
    }

    private void createWeaponsTab(Ship ship) {
        Window window = new Window("Weapons", skin);
        window.setMovable(false);
        window.setModal(false);
        window.defaults().spaceBottom(10);


        for (final Turret wep : ship.combatSystemsControl.getComponents(Turret.class)) {
            window.row().fill().expandX();
            Label label = new Label(wep.getName(), skin);
            ImageTextButton button = new ImageTextButton("", skin);
            button.getImage().setDrawable(new TextureRegionDrawable(Resources.getInstance().getTexture("weapon")));


            button.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent changeEvent, Actor actor) {
                    ((ImageTextButton) actor).setChecked(!wep.isActive());
                    wep.setActive(!wep.isActive());
                }
            });
            window.add(label);
            window.row().fill().getExpandX();
            window.add(button);
        }
        window.setPosition(0, 0);
        window.pack();
        main.addActor(window);
        loaded = true;
    }

    @Override
    public void render(float delta) {
        if (!loaded && Player.getInstance().getShip() != null) {
            createWeaponsTab(Player.getInstance().getShip());
        }
        if (gestures.isTargetPointSet()) {
            targetPointRenderable.setPosition(gestures.getTargetPoint());
            targetPointRenderable.draw(mainBatch);
        }
        gestures.update(delta);
    }

    @Override
    public void dispose() {
        skin.dispose();
        super.dispose();
    }
}
