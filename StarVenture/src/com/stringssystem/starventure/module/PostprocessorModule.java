package com.stringssystem.starventure.module;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.stringssystem.starventure.postprocess.PostProcessor;
import com.stringssystem.starventure.postprocess.effects.Bloom;
import com.stringssystem.starventure.postprocess.effects.CrtMonitor;
import com.stringssystem.starventure.postprocess.filters.CrtScreen;

/**
 * Created by jakub.coufal@ibacz.eu on 2.7.2014.
 */
public class PostprocessorModule extends AbstractGameModule {

    private PostProcessor postprocessor;
    private boolean initedEffects = false;

    @Override
    public void init(SpriteBatch mainBatch, Stage stage) {
        postprocessor = new PostProcessor(true, true, true);
        super.init(mainBatch, stage);
    }

    @Override
    public void render(float delta) {

        if (!initedEffects) {
            postprocessor.addEffect(new Bloom((int) stage.getWidth(), (int) stage.getHeight()));
            postprocessor.addEffect(new CrtMonitor((int) stage.getWidth(), (int) stage.getHeight(),
                    true, true, CrtScreen.RgbMode.ChromaticAberrations));
            initedEffects = true;
        }

        postprocessor.render();
    }
}
