package com.stringssystem.starventure.module;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.physics.box2d.*;
import com.stringssystem.starventure.api.FixtureType;
import com.stringssystem.starventure.api.Projectile;
import com.stringssystem.starventure.ship.Ship;

import java.util.*;

public final class CollisionModule extends AbstractGameModule implements ContactListener {

    private final Map<Ship, Set<Projectile>> collidables = new HashMap<Ship, Set<Projectile>>();
    private boolean isBound = false;

    @Override
    public void render(float delta) {
        if (!isBound) {
            ModuleManager.getModule(WorldModule.class).getSimulatedWorld().setContactListener(this);
            isBound = true;
        }
        for (Map.Entry<Ship, Set<Projectile>> candidateSet : collidables.entrySet()) {
            Ship ship = candidateSet.getKey();
            for (Projectile projectile : candidateSet.getValue()) {
                solveCollision(ship, projectile);
            }
        }

    }

    private void solveCollision(Ship ship, Projectile projectile) {
        if (ship != null && projectile != null && projectile.isAlive() && Intersector.isPointInPolygon(
                Arrays.asList(ship.shipHull.getBoxCollisionPolygon().items),
                projectile.getBoxPosition())) {
            ModuleManager.getModule(ParticleEffectsModule.class).mediumExplosionHit(projectile);
            ship.damage(projectile.getDamage());
            projectile.setAlive(false);
        }
    }

    @Override
    public int getPosition() {
        return 20;
    }

    private boolean addColidables(Fixture a, Fixture b) {
        if (a.getUserData().equals(FixtureType.WRAPPER) && b.getUserData().equals(FixtureType.PROJECTILE)) {
            Ship ship = (Ship) a.getBody().getUserData();
            Projectile projectile = (Projectile) b.getBody().getUserData();
            if (!collidables.containsKey(ship)) {
                collidables.put(ship, new HashSet<Projectile>());
            }
            collidables.get(ship).add(projectile);
            return true;
        }
        return false;
    }

    private void addColidables(Contact contact) {
        if (!addColidables(contact.getFixtureA(), contact.getFixtureB())) {
            addColidables(contact.getFixtureB(), contact.getFixtureA());
        }
    }

    private boolean removeCollidables(Fixture a, Fixture b) {
        if (a.getUserData().equals(FixtureType.WRAPPER) && b.getUserData().equals(FixtureType.PROJECTILE)) {
            Ship ship = (Ship) a.getBody().getUserData();
            Projectile projectile = (Projectile) b.getBody().getUserData();
            if (collidables.get(ship).size() == 1) {
                collidables.remove(ship);
                return true;
            }
            collidables.get(ship).remove(projectile);
            return true;
        }
        return false;
    }

    private void removeCollidables(Contact contact) {
        if (!removeCollidables(contact.getFixtureA(), contact.getFixtureB())) {
            removeCollidables(contact.getFixtureB(), contact.getFixtureA());
        }
    }

    @Override
    public void beginContact(Contact contact) {
        addColidables(contact);
    }

    @Override
    public void endContact(Contact contact) {
        removeCollidables(contact);
    }

    @Override
    public void preSolve(Contact contact, Manifold manifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse contactImpulse) {
    }
}
