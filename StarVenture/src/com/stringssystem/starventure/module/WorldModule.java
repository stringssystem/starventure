package com.stringssystem.starventure.module;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.DelayedRemovalArray;
import com.badlogic.gdx.utils.Disposable;
import com.stringssystem.starventure.api.ContainsRenderables;
import com.stringssystem.starventure.api.GameObject;
import com.stringssystem.starventure.api.Renderable;

import java.util.PriorityQueue;
import java.util.Queue;

public final class WorldModule extends AbstractGameModule {

    private final World simulatedWorld = new World(new Vector2(0, 0), true);
    private final Queue<Renderable> renderables = new PriorityQueue<Renderable>();
    private float step = Gdx.app.getType() == Application.ApplicationType.Desktop ? 60 : 60;
    private final DelayedRemovalArray<GameObject> gameObjects = new DelayedRemovalArray<GameObject>();

    public Body createBody(BodyDef bodyDef, GameObject gameObject) {
        gameObjects.add(gameObject);
        if (gameObject instanceof ContainsRenderables) {
            for (Renderable dw : ((ContainsRenderables) gameObject).getRenderables()) {
                renderables.offer(dw);
            }
        }
        Body ret = simulatedWorld.createBody(bodyDef);
        ret.setUserData(gameObject);
        return ret;
    }

    public void destroyBody(GameObject gameObject) {
        simulatedWorld.destroyBody(gameObject.getBody());
        gameObject.getBody().setUserData(null);
        gameObjects.removeValue(gameObject, true);
        if (gameObject instanceof ContainsRenderables) {
            for (Renderable dw : ((ContainsRenderables) gameObject).getRenderables()) {
                renderables.remove(dw);
            }
        }
        if (gameObject instanceof Disposable) {
            ((Disposable) gameObject).dispose();
        }
    }

    public World getSimulatedWorld() {
        return simulatedWorld;
    }

    public Array<GameObject> getGameObjects() {
        return gameObjects;
    }

    @Override
    public void render(float delta) {
        gameObjects.begin();
        for (int i = 0; i < gameObjects.size; i++) {
            GameObject go = gameObjects.get(i);
            if (go.isAlive()) {
                go.update(delta);
            } else {
                destroyBody(go);
            }
        }
        simulatedWorld.step(1 / step, 10, 10);
        gameObjects.end();

        if (!renderables.isEmpty()) {
            for (Renderable renderable : renderables) {
                renderable.draw(mainBatch);
            }
        }
    }

    @Override
    public int getPosition() {
        return 50;
    }
}