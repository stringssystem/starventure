package com.stringssystem.starventure.module;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.stringssystem.starventure.api.GameModule;
import com.stringssystem.starventure.util.MapUtil;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 09.08.13
 * Time: 10:56
 * To change this template use File | Settings | File Templates.
 */
public final class ModuleManager {

    private static ModuleManager instance;
    private static SpriteBatch batch;
    private static Stage stage;
    private Map<Class<? extends GameModule>, GameModule> unsortedModules =
            new HashMap<Class<? extends GameModule>, GameModule>();
    private Map<Class<? extends GameModule>, GameModule> sortedModules =
            new TreeMap<Class<? extends GameModule>, GameModule>();

    private static ModuleManager getInstance() {
        if (instance == null) {
            instance = new ModuleManager();
        }
        return instance;
    }

    public static void registerModule(GameModule module) {
        module.init(batch, stage);
        getInstance().unsortedModules.put(module.getClass(), module);
        getInstance().sortedModules = MapUtil.sortMapByValue(getInstance().unsortedModules);
    }

    public static void registerModules(GameModule... modules) {
        for (GameModule mod : modules) {
            getInstance().unsortedModules.put(mod.getClass(), mod);
            mod.init(batch, stage);
        }
        getInstance().sortedModules = MapUtil.sortMapByValue(getInstance().unsortedModules);
    }

    public static <T extends GameModule> T getModule(Class<T> moduleClass) {
        GameModule gameModule = getInstance().sortedModules.get(moduleClass);
        if (gameModule == null)
            throw new IllegalArgumentException("Module not found!");
        if (!gameModule.isInitialized())
            gameModule.init(batch, stage);
        return (T) gameModule;
    }

    public static Collection<GameModule> getAllModules() {
        return Collections.unmodifiableCollection(getInstance().sortedModules.values());
    }

    public static void init(SpriteBatch batch, Stage stage) {
        ModuleManager.batch = batch;
        ModuleManager.stage = stage;
        for (GameModule module : getInstance().sortedModules.values()) {
            if (!module.isInitialized()) {
                module.init(batch, stage);
            }
        }
    }

    public static boolean isAllModulesInitialized() {
        boolean retVal = true;
        for (GameModule module : getInstance().sortedModules.values()) {
            retVal = retVal && module.isInitialized();
        }
        return retVal;
    }

    public static void dispose() {
        for (GameModule mod : getAllModules()) {
            mod.dispose();
        }
        instance = null;
    }
}
