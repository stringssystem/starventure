package com.stringssystem.starventure.module;

import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.stringssystem.starventure.Resources;
import com.stringssystem.starventure.api.Projectile;
import com.stringssystem.starventure.object.GameObjectImpl;
import com.stringssystem.starventure.particlesystem.ExplosionParticleEmitter;
import com.stringssystem.starventure.particlesystem.SparkParticleEmitter;
import com.stringssystem.starventure.ship.Ship;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 07.07.13
 * Time: 15:22
 * To change this template use File | Settings | File Templates.
 */
public final class ParticleEffectsModule extends AbstractGameModule {

    private SparkParticleEmitter sparkParticles = new SparkParticleEmitter();
    private ExplosionParticleEmitter explosionParticles = new ExplosionParticleEmitter();
    private Map<String, ParticleEffectPool> effects = new HashMap<String, ParticleEffectPool>();
    private Array<ParticleEffectPool.PooledEffect> activeEffects = new Array<ParticleEffectPool.PooledEffect>();

    public ParticleEffectPool.PooledEffect getEffect(String name) {
        if (!effects.containsKey(name)) {
            effects.put(name, new ParticleEffectPool(Resources.getInstance().getEffect(name), 5, 20));
        }
        ParticleEffectPool.PooledEffect retEffect = effects.get(name).obtain();
        activeEffects.add(retEffect);
        return retEffect;
    }

    public void freeEffect(ParticleEffectPool.PooledEffect effect) {
        if (activeEffects.contains(effect, true)) {
            effect.free();
            activeEffects.removeValue(effect, true);
        }
    }

    public void bulletHit(Ship ship, Projectile bullet) {
        bullet.getRotationInVector();
        Vector2 pos = new Vector2().set(bullet.getWorldPosition().x + (bullet.getRotationInVector().x), bullet.getWorldPosition().y + (bullet.getRotationInVector().y));

        // ugh. . .
        Vector2 bullet_vel = new Vector2().set(bullet.getWorldPosition());

        Vector2 bullet_dir;
        if (bullet_vel.dot(bullet_vel) == 0) {
            bullet_dir = new Vector2();
        } else {
            bullet_dir = bullet_vel.nor();
        }
        Vector2 vel = new Vector2(bullet_dir.x * 1.5f, bullet_dir.y * 1.5f);

        // if (bullet instanceof Laser) {
        //     laserHit(pos, vel);
        // } else if (bullet instanceof Bomb) {
        mediumExplosionHit(bullet);
        // } else if (bullet instanceof Missile) {
        //     tinyExplosionHit(bullet.getPosition());
        // }
    }

    public void explode(GameObjectImpl ship) {
        explode(ship, ship.getWorldPosition());
    }

    void explode(GameObjectImpl ship, Vector2 pos) {
        explosionParticles.addBigExplosion(pos);
    }

    public void laserHit(Vector2 pos, Vector2 vel) {
        sparkParticles.addLaserExplosion(pos, vel);
    }

    void mediumExplosionHit(Projectile projectile) {
        explosionParticles.addMediumExplosion(projectile.getWorldPosition());
    }

    public void tinyExplosionHit(Projectile projectile) {
        explosionParticles.addTinyExplosion(projectile.getWorldPosition());
    }

    @Override
    public void dispose() {
        sparkParticles.dispose();
        explosionParticles.dispose();
        for (ParticleEffectPool.PooledEffect eff : activeEffects) {
            eff.dispose();
        }
        super.dispose();
    }

    @Override
    public void render(float delta) {
        sparkParticles.draw(mainBatch);
        explosionParticles.draw(mainBatch);
        for (ParticleEffectPool.PooledEffect eff : activeEffects) {
            eff.draw(mainBatch, delta);
        }
    }

    @Override
    public int getPosition() {
        return 51;
    }
}
