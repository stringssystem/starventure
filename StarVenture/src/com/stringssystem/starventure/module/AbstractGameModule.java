package com.stringssystem.starventure.module;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.stringssystem.starventure.api.GameModule;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 09.08.13
 * Time: 10:48
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractGameModule implements GameModule {

    protected SpriteBatch mainBatch;
    protected int width = 800;
    protected int height = 400;
    protected Stage stage;

    @Override
    public int getPosition() {
        return 0;
    }

    @Override
    public void init(SpriteBatch mainBatch, Stage stage) {
        this.mainBatch = mainBatch;
        this.stage = stage;
    }

    @Override
    public void dispose() {
        this.stage = null;
        this.mainBatch = null;
    }

    @Override
    public boolean isInitialized() {
        return mainBatch != null && stage != null;
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public int compareTo(GameModule gameModule) {
        if (this.getPosition() == gameModule.getPosition())
            return 0;
        if (this.getPosition() > gameModule.getPosition())
            return 1;
        else return -1;
    }
}
