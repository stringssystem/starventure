package com.stringssystem.starventure;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.stringssystem.starventure.screen.LoadingScreen;

public class StarVentureGame extends Game {

    @Override
    public void create() {
        //Gdx.input.setCatchBackKey(true);
        Gdx.gl.glDisable(GL20.GL_CULL_FACE);
        Gdx.gl.glDisable(GL20.GL_DEPTH_TEST);
        Gdx.graphics.setDisplayMode(
                Settings.getResolutionSelectedWidth(), Settings.getResolutionSelectedHeight(), false);
        Gdx.graphics.setVSync(Settings.getVsyncEnabled());
        setScreen(new LoadingScreen(this));
    }


}
