package com.stringssystem.starventure.util;

import com.badlogic.gdx.math.Vector2;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 15.08.13
 * Time: 12:23
 * To change this template use File | Settings | File Templates.
 */
public class Box2dUtil {

    private static final float BOX_TO_WORLD = 100.0f;
    private static final float WORLD_TO_BOX = 0.01f;

    public static float ConvertToBox(float x) {
        return x * WORLD_TO_BOX;
    }

    public static float ConvertToWorld(float x) {
        return x * BOX_TO_WORLD;
    }

    public static Vector2 ConvertToBox(Vector2 vec) {
        return vec.set(vec.x * WORLD_TO_BOX, vec.y * WORLD_TO_BOX);
    }

    public static Vector2 ConvertToWorld(Vector2 vec) {
        return vec.set(vec.x * BOX_TO_WORLD, vec.y * BOX_TO_WORLD);
    }


}
