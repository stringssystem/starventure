package com.stringssystem.starventure.util;

import java.util.Comparator;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 12.08.13
 * Time: 15:12
 * To change this template use File | Settings | File Templates.
 */
public class MapUtil {


    /*
     * Util method that:
     * 1. Constructs a sorted map object providing a Comparator class
     * 2. Populating the sorted map by calling putAll with the UNsorted
     *     data.
     * 3. When the putAll is called, the comparator is used to do the
     *     ordering.
     * 4. The method returns the sorted map.
     */
    public static <K, V extends Comparable<V>> Map<K, V> sortMapByValue(Map<K, V> unsortedMap) {
        SortedMap<K, V> sortedMap = new TreeMap<K, V>(new ValueComparer<K, V>(unsortedMap));
        sortedMap.putAll(unsortedMap);
        return sortedMap;
    }

    /*
     * An inner class that implements a Comparator to compare the values inside the map.
     * Constructor takes the contentes of the UNsorted map.
     */
    private static class ValueComparer<K, V extends Comparable<V>> implements Comparator<K> {

        private final Map<K, V> map;

        public ValueComparer(Map<K, V> map) {
            super();
            this.map = map;
        }

        public int compare(K key1, K key2) {
            V value1 = this.map.get(key1);
            V value2 = this.map.get(key2);
            int c = value1.compareTo(value2);
            if (c != 0) {
                return c;
            }
            Integer hashCode1 = key1.hashCode();
            Integer hashCode2 = key2.hashCode();
            return hashCode1.compareTo(hashCode2);
        }
    }

    /*
     * Utility method to print a map using an Iterator.
     */
    private static void printMap(Map<?, ?> data) {
        for (Object key : data.keySet()) {
            System.out.println("Value/key:" + data.get(key).toString() + "/" + key.toString());
        }
    }
}
