package com.stringssystem.starventure.util;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 02.08.13
 * Time: 12:02
 * To change this template use File | Settings | File Templates.
 */

public class JSONUtil {

    public static void writeVector2(String vectorName, Json json, Vector2 vector) {
        json.writeObjectStart(vectorName);
        json.writeValue("x", vector.x);
        json.writeValue("y", vector.y);
        json.writeObjectEnd();
    }

    @SuppressWarnings("unchecked")
    public static Vector2 readVector2(String vectorName, OrderedMap<String, Object> jsonMap) {
        OrderedMap<String, Object> vect = (OrderedMap<String, Object>) jsonMap.get(vectorName);
        return new Vector2((Float) vect.get("x", 0f), (Float) vect.get("y", 0f));
    }

    public static Vector2 readVector2(OrderedMap<String, Object> jsonMap) {
        return new Vector2((Float) jsonMap.get("x", 0f), (Float) jsonMap.get("y", 0f));
    }
}

