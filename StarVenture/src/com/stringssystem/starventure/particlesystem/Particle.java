package com.stringssystem.starventure.particlesystem;

import com.badlogic.gdx.math.Vector2;

public class Particle {

    protected float life;
    protected Vector2 position = new Vector2();
    protected Vector2 velocity = new Vector2();
    protected float scale;

    public Particle() {
    }

    public void setup(Vector2 position, Vector2 velocity, float life, float scale) {
        this.position.set(position);
        this.velocity.set(velocity);
        this.life = life;
        this.scale = scale;
    }

}
