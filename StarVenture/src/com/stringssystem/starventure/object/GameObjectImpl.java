package com.stringssystem.starventure.object;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.stringssystem.starventure.api.FixtureBindable;
import com.stringssystem.starventure.api.GameObject;
import com.stringssystem.starventure.module.ModuleManager;
import com.stringssystem.starventure.module.WorldModule;
import com.stringssystem.starventure.util.Box2dUtil;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 15.08.13
 * Time: 12:05
 * To change this template use File | Settings | File Templates.
 */
public class GameObjectImpl implements GameObject {

    private long id = IdGenerator.getNewId();
    private Body body;
    private boolean alive;
    private Vector2 worldPosition = new Vector2();
    private Vector2 worldFacing = new Vector2(1, 1);
    private Map<FixtureDef, Object> fixtureDefs = new LinkedHashMap<FixtureDef, Object>();


    public FixtureDef addFixtureDef(FixtureDef def, Object userData) {
        fixtureDefs.put(def, userData);
        return def;
    }

    public FixtureDef addFixtureDef(Shape shape, float density, Object userData) {
        FixtureDef def = new FixtureDef();
        def.shape = shape;
        def.density = density;
        fixtureDefs.put(def, userData);
        return def;
    }

    public FixtureDef addFixtureDef(FixtureDef def) {
        return addFixtureDef(def, null);
    }

    public FixtureDef addFixtureDef(Shape shape, float density) {
        return addFixtureDef(shape, density, null);
    }

    Fixture addFixture(FixtureDef def) {
        return body.createFixture(def);
    }

    protected Body makeBody(BodyDef.BodyType bodyType, Vector2 pos, float angle) {
        BodyDef def = new BodyDef();
        def.type = bodyType;
        def.position.set(Box2dUtil.ConvertToBox(pos.x), Box2dUtil.ConvertToBox(pos.y));
        def.angle = angle * MathUtils.degreesToRadians;
        body = ModuleManager.getModule(WorldModule.class).createBody(def, this);
        worldPosition.set(Box2dUtil.ConvertToWorld(body.getPosition().x),
                Box2dUtil.ConvertToWorld(body.getPosition().y));
        for (Map.Entry<FixtureDef, Object> fix : fixtureDefs.entrySet()) {
            Fixture fixture = addFixture(fix.getKey());
            Object val = fix.getValue();
            if (val != null) {
                fixture.setUserData(val);
                if (val instanceof FixtureBindable) {
                    ((FixtureBindable) val).setFixture(fixture);
                }
            }
        }
        return body;
    }

    @Override
    public Vector2 getWorldPosition() {
        return worldPosition;
    }

    private void updateWorldPositionAndFacing() {
        Vector2 tmp = body.getPosition();
        worldPosition.set(Box2dUtil.ConvertToWorld(tmp.x), Box2dUtil.ConvertToWorld(tmp.y));
        worldFacing.setAngle(body.getAngle() * MathUtils.radiansToDegrees);
    }

    @Override
    public Vector2 getRotationInVector() {
        return worldFacing.nor();
    }

    @Override
    public void setWorldPosition(Vector2 worldPosition) {
        float wx = Box2dUtil.ConvertToBox(worldPosition.x);
        float wy = Box2dUtil.ConvertToBox(worldPosition.y);
        float angle = body.getAngle();
        body.setTransform(wx, wy, angle);
        updateWorldPositionAndFacing();
    }

    @Override
    public void setRotationInDegrees(float degrees) {
        body.setTransform(body.getPosition(), MathUtils.degreesToRadians * degrees);
    }

    public float getRotationInDegrees() {
        return body.getAngle() * MathUtils.radiansToDegrees;
    }

    @Override
    public Vector2 getBoxPosition() {
        return body.getPosition();
    }

    @Override
    public void setBoxPosition(Vector2 boxPosition) {
        float angle = body.getAngle();
        body.setTransform(boxPosition.x, boxPosition.y, angle);
    }

    public Body getBody() {
        return body;
    }

    @Override
    public long getId() {
        return id;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

    @Override
    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    @Override
    public void update(float delta) {
        if (alive) {
            updateWorldPositionAndFacing();
        }
    }

    @Override
    public void spawn(Vector2 position, float facing) {
        makeBody(BodyDef.BodyType.DynamicBody, position.cpy(), facing);
        alive = true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameObject)) return false;

        GameObjectImpl that = (GameObjectImpl) o;

        return id == that.getId();

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
