package com.stringssystem.starventure.object;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.stringssystem.starventure.Resources;
import com.stringssystem.starventure.api.Layer;

public class TextureRegionRenderable extends AbstractRenderable {

    private boolean texNeedToInit = false;
    private Texture texture;
    private TextureRegion region;
    private String spriteName;

    public TextureRegionRenderable(TextureRegion tex, Layer layer) {
        setTexture(tex);
        scaleX = 1;
        scaleY = 1;
        color = Color.WHITE;
        setFilter(TextureFilter.Linear, TextureFilter.Linear);
        this.layer = layer;
        texNeedToInit = false;
    }

    public TextureRegionRenderable(String spriteName, Layer layer) {
        this.spriteName = spriteName;
        color = Color.WHITE;
        scaleX = 1;
        scaleY = 1;
        this.layer = layer;
        texNeedToInit = true;
    }

    public void setFilter(TextureFilter min, TextureFilter max) {
        texture.setFilter(min, max);
    }

    public void setTexture(TextureRegion region, int width, int height) {
        this.region = region;
        destWidth = width;
        destHeight = height;
        texture = region.getTexture();
        setOrigin(width / 2, height / 2);
    }

    void setTexture(TextureRegion region) {
        setTexture(region, region.getRegionWidth(), region.getRegionHeight());
    }

    private void initTexture() {
        if (!spriteName.isEmpty()) {
            setTexture(Resources.getInstance().getTexture(spriteName));
            setScale(scaleX, scaleY);
            setFilter(TextureFilter.Linear, TextureFilter.Linear);
            texNeedToInit = false;
        }
    }

    @Override
    public void draw(SpriteBatch sp) {
        if (texNeedToInit)
            initTexture();
        sp.setColor(color);
        sp.draw(region,
                position.x - destWidth / 2,
                position.y - destHeight / 2,
                originX, originY,
                destWidth, destHeight,
                scaleX, scaleY,
                rotation);
        sp.setColor(Color.WHITE);
    }

    @Override
    public void dispose() {
        texture.dispose();
    }

}
