package com.stringssystem.starventure.object;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.api.Layer;
import com.stringssystem.starventure.api.Renderable;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 06.09.13
 * Time: 15:04
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractRenderable implements Renderable {
    protected float rotation;
    protected float scaleX;
    protected float scaleY;
    protected float originX;
    protected float originY;
    protected Color color;
    protected Layer layer = Layer.BACKGROUND;
    protected int destWidth;
    protected int destHeight;
    protected Vector2 position = new Vector2();


    @Override
    public Layer getLayer() {
        return layer;
    }

    @Override
    public void setLayer(Layer layer) {
        this.layer = layer;
    }

    @Override
    public int getWidth() {
        return destWidth;
    }

    @Override
    public int getHeight() {
        return destHeight;
    }

    void setOrigin(int originx, int originy) {
        originX = originx;
        originY = originy;
    }

    @Override
    public void setDimension(int width, int height) {
        destWidth = width;
        destHeight = height;
        originX = width / 2;
        originY = height / 2;
    }

    @Override
    public void setColor(Color c) {
        this.color = c;
    }

    @Override
    public void setScale(float x, float y) {
        scaleX = x;
        scaleY = y;
    }

    @Override
    public int compareTo(Renderable renderable) {
        if (this.layer.ordinal() == renderable.getLayer().ordinal())
            return 0;
        return this.layer.ordinal() < renderable.getLayer().ordinal() ? -1 : 1;
    }

    @Override
    public float getRotation() {
        return rotation;
    }

    @Override
    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public void setPosition(Vector2 position) {
        this.position = position;
    }
}
