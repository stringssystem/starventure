package com.stringssystem.starventure.object;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.stringssystem.starventure.Resources;
import com.stringssystem.starventure.api.Layer;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 06.09.13
 * Time: 15:02
 * To change this template use File | Settings | File Templates.
 */
public class AnimationRenderable extends AbstractRenderable {

    private final float frameDuration;
    private String[] frameNames;
    private TextureRegion[] frames;
    private Animation animation;
    private float stateTime;
    private boolean animNeedToInit = true;

    public AnimationRenderable(float frameDuration, Layer layer, TextureRegion... frames) {
        this.color = Color.WHITE;
        this.scaleX = 1;
        this.scaleY = 1;
        this.layer = layer;
        this.frameDuration = frameDuration;
        this.frames = Arrays.copyOf(frames, frames.length);
    }

    public AnimationRenderable(float frameDuration, Layer layer, String... frameNames) {
        this.color = Color.WHITE;
        this.scaleX = 1;
        this.scaleY = 1;
        this.layer = layer;
        this.frameDuration = frameDuration;
        this.frameNames = Arrays.copyOf(frameNames, frameNames.length);
    }

    @Override
    public void draw(SpriteBatch sp) {
        stateTime += Gdx.graphics.getDeltaTime();
        if (animNeedToInit)
            init();
        TextureRegion currentFrame = animation.getKeyFrame(stateTime, true);
        sp.setColor(color);
        sp.draw(currentFrame,
                position.x - destWidth / 2,
                position.y - destHeight / 2,
                originX, originY,
                destWidth, destHeight,
                scaleX, scaleY,
                rotation);
        sp.setColor(Color.WHITE);
    }

    private void init() {
        if (frames == null) {
            frames = new TextureRegion[frameNames.length];
            for (int i = 0; i < frameNames.length; i++) {
                String frameName = frameNames[i];
                if (!frameName.isEmpty()) {
                    frames[i] = Resources.getInstance().getTexture(frameName);
                }
            }
        }
        if (frames[0] != null) {
            destWidth = frames[0].getRegionWidth();
            destHeight = frames[0].getRegionHeight();
            originX = destWidth / 2;
            originY = destHeight / 2;
        }
        animation = new Animation(frameDuration, frames);
        animNeedToInit = false;
    }

    @Override
    public void dispose() {
    }
}
