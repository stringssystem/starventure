package com.stringssystem.starventure.object;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 01.08.13
 * Time: 9:25
 * To change this template use File | Settings | File Templates.
 */
final class IdGenerator {

    private static final AtomicLong generator = new AtomicLong(0);

    private IdGenerator() {
    }

    public static long getNewId() {
        return generator.getAndIncrement();
    }
}
