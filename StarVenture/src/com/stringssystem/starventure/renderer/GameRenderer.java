package com.stringssystem.starventure.renderer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.stringssystem.starventure.Constants;
import com.stringssystem.starventure.Player;
import com.stringssystem.starventure.Resources;
import com.stringssystem.starventure.api.ContainsRenderables;
import com.stringssystem.starventure.api.GameModule;
import com.stringssystem.starventure.api.GameObject;
import com.stringssystem.starventure.api.Renderer;
import com.stringssystem.starventure.module.ModuleManager;
import com.stringssystem.starventure.module.PostprocessorModule;
import com.stringssystem.starventure.module.WorldModule;
import com.stringssystem.starventure.postprocess.PostProcessor;
import com.stringssystem.starventure.ship.Ship;


/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 08.07.13
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
public class GameRenderer implements Renderer, Disposable {

    private final SpriteBatch gameBatch;
    private final GameCamera cam = GameCamera.getInstance();
    private final ShapeRenderer debugRenderer = new ShapeRenderer();
    private final Texture background;
    private final FPSLogger fpsLogger;
    private final Box2DDebugRenderer box2DdebugRenderer = new Box2DDebugRenderer();
    private final Stage stage;
    private float width;
    private float height;

    public GameRenderer() {
        gameBatch = new SpriteBatch();
        background = Resources.getInstance().getRandomBackground();
        fpsLogger = new FPSLogger();
        stage = new Stage(0, 0, false, gameBatch);
        ModuleManager.init(gameBatch, stage);
    }

    @Override
    public void resize(int width, int height) {
        this.width = width;
        this.height = height;
        cam.resize(width, height);
        stage.setViewport(width, height, false);
        gameBatch.setProjectionMatrix(cam.getCombinedMatrix());
        for (GameModule mod : ModuleManager.getAllModules()) {
            mod.resize(width, height);
        }
    }

    void updateCamera(Ship ship) {
        float tX = ship.getWorldPosition().x - cam.getPosition().x;
        float tY = ship.getWorldPosition().y - cam.getPosition().y;
        cam.setPosition(tX, tY);
        gameBatch.setProjectionMatrix(cam.getCombinedMatrix());
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        gameBatch.begin();
        gameBatch.getProjectionMatrix().setToOrtho2D(0, 0, width, height);
        gameBatch.draw(background, 0, 0);
        gameBatch.end();
        Ship ship = Player.getInstance().getShip();
        if (ship != null) {
            updateCamera(ship);
        }
        gameBatch.begin();
        for (GameModule mod : ModuleManager.getAllModules()) {
            mod.render(Gdx.graphics.getDeltaTime());
        }
        gameBatch.end();
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        if (Constants.DEBUG)
            drawDebug();
        fpsLogger.log();
    }

    private void drawDebug() {
        gameBatch.begin();
        Matrix4 mat = cam.getCombinedMatrix();
        mat.scale(Constants.BOX_TO_WORLD, Constants.BOX_TO_WORLD, 1f);
        box2DdebugRenderer.render(ModuleManager.getModule(WorldModule.class).getSimulatedWorld(), mat);
        mat.scale(Constants.WORLD_TO_BOX, Constants.WORLD_TO_BOX, 1f);
        debugRenderer.setProjectionMatrix(cam.getCombinedMatrix());
        Array<GameObject> it = ModuleManager.getModule(WorldModule.class).getGameObjects();
        for (GameObject block : it) {
            if (block instanceof ContainsRenderables) {
                debugRenderer.begin(ShapeRenderer.ShapeType.Point);
                debugRenderer.setColor(Color.MAGENTA);
                debugRenderer.point(block.getWorldPosition().x, block.getWorldPosition().y, 0);
                debugRenderer.end();
            }
            debugRenderer.begin(ShapeRenderer.ShapeType.Point);
            debugRenderer.setColor(Color.BLUE);
            debugRenderer.point(block.getWorldPosition().x, block.getWorldPosition().y, 0);
            debugRenderer.end();
            if (block instanceof Ship) {
                Ship ship = (Ship) block;
                debugRenderer.begin(ShapeRenderer.ShapeType.Point);
                debugRenderer.setColor(Color.CYAN);
                Array<Vector2> pol = ship.shipHull.getWorldCollisionPolygon();
                for (Vector2 p : pol) {
                    debugRenderer.point(p.x, p.y, 0);
                }
                debugRenderer.end();
            }
        }
        gameBatch.end();

    }


    @Override
    public void dispose() {
        stage.dispose();
        gameBatch.dispose();
    }
}

