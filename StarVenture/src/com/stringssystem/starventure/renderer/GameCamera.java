package com.stringssystem.starventure.renderer;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 21.08.13
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
public class GameCamera {

    private static GameCamera instance;
    private OrthographicCamera cam = new OrthographicCamera(800, 400);
    private static final Vector3 tmp = new Vector3();

    public static GameCamera getInstance() {
        if (instance == null)
            instance = new GameCamera();
        return instance;
    }

    public OrthographicCamera getCamera() {
        return cam;
    }

    public Vector2 getPosition() {
        return new Vector2(cam.position.x, cam.position.y);
    }

    public void setPosition(Vector2 position) {
        cam.translate(position.x, position.y, 0);
        cam.update();
    }

    public void setPosition(float x, float y) {
        cam.translate(x, y, 0);
        cam.update();
    }

    public void resize(int width, int height) {
        cam = new OrthographicCamera(width, height);
        cam.position.x = width / 2;
        cam.position.y = height / 2;
        cam.update();
    }

    public Matrix4 getCombinedMatrix() {
        return cam.combined;
    }

    public Vector2 screenToWorldCoords(Vector2 screenCoords) {
        tmp.set(screenCoords.x, screenCoords.y, 0);
        cam.unproject(tmp);
        return screenCoords.set(tmp.x, tmp.y);
    }

    public Vector2 worldToScreenCoords(Vector2 worldCoords) {
        tmp.set(worldCoords.x, worldCoords.y, 0);
        cam.project(tmp);
        return worldCoords.set(tmp.x, tmp.y);
    }

    public Vector2 screenToWorldCoords(float x, float y) {
        Vector3 tmpVec = new Vector3(x, y, 0);
        cam.unproject(tmpVec);
        return new Vector2(tmpVec.x, tmpVec.y);
    }

    public Vector2 worldToScreenCoords(float x, float y) {
        Vector3 tmpVec = new Vector3(x, y, 0);
        cam.project(tmpVec);
        return new Vector2(tmpVec.x, tmpVec.y);
    }
}
