package com.stringssystem.starventure.controls;

import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.Player;
import com.stringssystem.starventure.renderer.GameCamera;
import com.stringssystem.starventure.ship.Ship;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 08.07.13
 * Time: 16:32
 * To change this template use File | Settings | File Templates.
 */
public class GestureInputProcessor implements GestureDetector.GestureListener {

    public static final int ALLOWED_DIFF = 30;
    private float initialScale = 1;
    private Vector2 targetPoint = new Vector2();
    private boolean targetPointSet = false;

    public Vector2 getTargetPoint() {
        return targetPoint;
    }

    public boolean isTargetPointSet() {
        return targetPointSet;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int i2) {
        initialScale = GameCamera.getInstance().getCamera().zoom;
        return true;
    }

    @Override
    public boolean tap(float x, float y, int count, int i2) {
        GameCamera.getInstance().screenToWorldCoords(targetPoint.set(x, y));
        targetPointSet = true;
        return true;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int i) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        float ratio = initialDistance / distance;
        GameCamera.getInstance().getCamera().zoom = initialScale * ratio;
        return true;
    }

    @Override
    public boolean pinch(Vector2 initialFirstPointer, Vector2 initialSecondPointer, Vector2 firstPointer, Vector2 secondPointer) {
        return false;
    }

    public void update(float delta) {
        Ship playerShip = Player.getInstance().getShip();
        if (targetPointSet) {
            if (playerShip.getWorldPosition().dst(targetPoint) > ALLOWED_DIFF) {
                playerShip.engineSystemsControl.goTowards(targetPoint, false);
            } else {
                playerShip.engineSystemsControl.stop();
                targetPointSet = false;
            }
        }
    }

}
