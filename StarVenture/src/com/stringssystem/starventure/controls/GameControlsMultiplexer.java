package com.stringssystem.starventure.controls;

import com.badlogic.gdx.InputMultiplexer;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 30.08.13
 * Time: 14:58
 * To change this template use File | Settings | File Templates.
 */
public class GameControlsMultiplexer {

    private static GameControlsMultiplexer instance;
    private InputMultiplexer multiplexer = new InputMultiplexer();

    public static GameControlsMultiplexer getInstance() {
        if (instance == null) {
            instance = new GameControlsMultiplexer();
        }
        return instance;
    }

    public InputMultiplexer getMultiplexer() {
        return multiplexer;
    }
}
