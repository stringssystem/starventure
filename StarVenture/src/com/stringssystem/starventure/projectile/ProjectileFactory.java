package com.stringssystem.starventure.projectile;

import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.OrderedMap;
import com.stringssystem.starventure.api.Factory;
import com.stringssystem.starventure.api.Layer;
import com.stringssystem.starventure.api.Projectile;
import com.stringssystem.starventure.api.Renderable;
import com.stringssystem.starventure.object.TextureRegionRenderable;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 07.08.13
 * Time: 9:52
 * To change this template use File | Settings | File Templates.
 */
public class ProjectileFactory implements Factory<Projectile>, Json.Serializable {

    private float damage = 0;
    private float bulletSpeed = 0f;
    private String spriteName = "";
    private float spriteScale = 0f;
    private float range = 200f;


    @Override
    public void write(Json json) {
        json.writeValue("damage", damage);
        json.writeValue("speed", bulletSpeed);
        json.writeValue("spriteName", spriteName);
        json.writeValue("spriteScale", spriteScale);
        json.writeValue("range", range);
    }

    @Override
    public void read(Json json, OrderedMap<String, Object> stringObjectOrderedMap) {
        damage = json.readValue("damage", Float.class, 10f, stringObjectOrderedMap);
        bulletSpeed = json.readValue("speed", Float.class, 0f, stringObjectOrderedMap);
        spriteName = json.readValue("spriteName", String.class, "", stringObjectOrderedMap);
        spriteScale = json.readValue("spriteScale", Float.class, 1f, stringObjectOrderedMap);
        range = json.readValue("range", Float.class, 200f, stringObjectOrderedMap);
    }

    @Override
    public Projectile newInstance() {
        Projectile projectile = new ProjectileImpl();
        projectile.setDamage(damage);
        projectile.setBulletSpeed(bulletSpeed);
        Renderable draw = new TextureRegionRenderable(spriteName, Layer.PROJECTILES);
        draw.setScale(spriteScale, spriteScale);
        projectile.setDrawable(draw);
        projectile.setRange(range);
        return projectile;
    }
}
