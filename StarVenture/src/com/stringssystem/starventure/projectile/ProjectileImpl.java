package com.stringssystem.starventure.projectile;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.stringssystem.starventure.api.*;
import com.stringssystem.starventure.object.GameObjectImpl;

public class ProjectileImpl extends GameObjectImpl implements Projectile, ContainsRenderables {

    private float damage = 0;
    private float bulletSpeed = 0f;
    private Vector2 launchPosition = new Vector2();
    private float range;
    private Array<Renderable> drawables = new Array<Renderable>();
    private Renderable projectileRenderable;

    public ProjectileImpl() {
        FixtureDef def = new FixtureDef();
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(0.001f, 0.001f);
        def.shape = shape;
        def.density = 0.001f;
        def.filter.categoryBits = GameObjectCategory.FRIENDLY_PROJECTILE.mask();
        def.filter.maskBits = GameObjectMask.ENEMY.mask();
        addFixtureDef(def, FixtureType.PROJECTILE);
    }

    @Override
    public float getRange() {
        return range;
    }

    @Override
    public void setRange(float range) {
        this.range = range;
    }

    @Override
    public void setDrawable(Renderable renderable) {
        projectileRenderable = renderable;
        drawables.add(renderable);
    }

    @Override
    protected Body makeBody(BodyDef.BodyType bodyType, Vector2 pos, float angle) {
        Body body = super.makeBody(bodyType, pos, angle);
        body.setBullet(true);
        return body;
    }

    @Override
    public void setDamage(float damage) {
        this.damage = damage;
    }

    @Override
    public float getBulletSpeed() {
        return bulletSpeed;
    }

    @Override
    public void setBulletSpeed(float bulletSpeed) {
        this.bulletSpeed = bulletSpeed;
    }

    @Override
    public float getDamage() {
        return damage;
    }

    @Override
    public void update(float delta) {
        super.update(delta);
        projectileRenderable.getPosition().set(getWorldPosition());
        projectileRenderable.setRotation(getRotationInDegrees());
        if (launchPosition.dst(getWorldPosition()) > range) {
            setAlive(false);
        }
    }

    @Override
    public void spawn(Vector2 position, float facing) {
        launchPosition.set(position);
        super.spawn(position, facing);
        position.nor().mul(bulletSpeed).setAngle(facing);
        this.getBody().setLinearVelocity(position.x, position.y);
    }

    @Override
    public Array<Renderable> getRenderables() {
        return drawables;
    }
}
