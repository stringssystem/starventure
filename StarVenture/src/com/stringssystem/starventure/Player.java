package com.stringssystem.starventure;

import com.stringssystem.starventure.ship.Ship;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 19.07.13
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 */
public class Player {

    private static Player instance;

    public static Player getInstance() {
        if (instance == null)
            instance = new Player();
        return instance;
    }

    private Ship ship;

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }
}
