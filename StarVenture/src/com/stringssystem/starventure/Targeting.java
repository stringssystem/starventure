package com.stringssystem.starventure;

import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.api.GameObject;
import com.stringssystem.starventure.module.ModuleManager;
import com.stringssystem.starventure.module.WorldModule;
import com.stringssystem.starventure.renderer.GameCamera;
import com.stringssystem.starventure.ship.Ship;

public class Targeting {

    public static boolean onScreen(Vector2 position) {
        return onScreen(position, 0);
    }

    public static boolean onScreen(Vector2 position, float buffer) {
        float width = GameCamera.getInstance().getCamera().viewportWidth;
        float height = GameCamera.getInstance().getCamera().viewportHeight;
        return position.x >= 0 - buffer && position.x <= width + buffer && position.y >= 0 - buffer
                && position.y <= height + buffer;
    }

    public static <T extends GameObject> T getTargetInRange(Vector2 from, float range, float arcWidth) {
        for (GameObject obj : ModuleManager.getModule(WorldModule.class).getGameObjects()) {
            if (obj instanceof Ship && obj.getWorldPosition().dst(from) <= range && ((Ship) obj).isEnemy()) {
                return (T) obj;
            }
        }
        return null;
    }


}
