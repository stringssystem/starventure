package com.stringssystem.starventure.api;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 02.09.13
 * Time: 8:39
 * To change this template use File | Settings | File Templates.
 */
public enum Layer {
    BACKGROUND,
    SHIPS,
    MODULES,
    PROJECTILES,
    EFFECTS
}
