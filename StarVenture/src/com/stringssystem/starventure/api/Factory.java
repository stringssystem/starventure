package com.stringssystem.starventure.api;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 05.08.13
 * Time: 23:54
 * To change this template use File | Settings | File Templates.
 */
public interface Factory<T> {

    public T newInstance();

}
