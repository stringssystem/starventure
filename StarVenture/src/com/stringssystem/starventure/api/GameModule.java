package com.stringssystem.starventure.api;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 09.08.13
 * Time: 10:41
 * To change this template use File | Settings | File Templates.
 */
public interface GameModule extends Disposable, Comparable<GameModule> {

    boolean isInitialized();

    void render(float delta);

    void resize(int width, int height);

    int getPosition();

    void init(SpriteBatch mainBatch, Stage stage);
}
