package com.stringssystem.starventure.api;

import com.badlogic.gdx.math.Vector2;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 07.07.13
 * Time: 16:21
 * To change this template use File | Settings | File Templates.
 */
public interface MissileAI extends AI {
    public Vector2 predict();
}
