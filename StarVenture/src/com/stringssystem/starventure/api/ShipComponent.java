package com.stringssystem.starventure.api;

import com.stringssystem.starventure.ship.Ship;
import com.stringssystem.starventure.ship.component.ComponentType;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 27.08.13
 * Time: 10:23
 * To change this template use File | Settings | File Templates.
 */
public interface ShipComponent {

    Ship getShip();

    void update();

    void setName(String name);

    String getName();

    ComponentType getType();

    void activate();

    void deactivate();

    boolean isActive();

    <T extends Hardpoint> T getHardpoint(Class<T> clazz);

    void setHardpoint(Hardpoint hardpoint);

    void setActive(boolean active);

    Hardpoint getHardpoint();

    void dispose();
}
