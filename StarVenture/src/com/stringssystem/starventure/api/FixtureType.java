package com.stringssystem.starventure.api;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 25.09.13
 * Time: 10:02
 * To change this template use File | Settings | File Templates.
 */
public enum FixtureType {
    HULL,
    WRAPPER,
    PROJECTILE
}
