package com.stringssystem.starventure.api;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 19.07.13
 * Time: 16:00
 * To change this template use File | Settings | File Templates.
 */
public interface Renderer {
    public void render();

    public void resize(int w, int h);
}
