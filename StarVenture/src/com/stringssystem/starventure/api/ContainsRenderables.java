package com.stringssystem.starventure.api;

import com.badlogic.gdx.utils.Array;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 13.07.13
 * Time: 14:02
 * To change this template use File | Settings | File Templates.
 */
public interface ContainsRenderables {
    Array<Renderable> getRenderables();
}
