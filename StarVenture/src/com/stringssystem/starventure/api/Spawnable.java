package com.stringssystem.starventure.api;

import com.badlogic.gdx.math.Vector2;

/**
 * Created with IntelliJ IDEA.
 * User: Maks
 * Date: 5.8.13
 * Time: 21:12
 * To change this template use File | Settings | File Templates.
 */
public interface Spawnable {
    public void spawn(Vector2 position, float facing);
}
