package com.stringssystem.starventure.api;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 01.09.13
 * Time: 16:05
 * To change this template use File | Settings | File Templates.
 */
public enum GameObjectMask {


    BOUNDARY(-1),
    FRIENDLY(
            GameObjectCategory.FRIENDLY_SHIP.mask() |
                    GameObjectCategory.FRIENDLY_PROJECTILE.mask()
    ),
    ENEMY(
            GameObjectCategory.ENEMY_SHIP.mask() |
                    GameObjectCategory.ENEMY_PROJECTILE.mask()
    );

    private int value;

    GameObjectMask(int value) {
        this.value = value;
    }

    public short mask() {
        return (short) value;
    }
}
