package com.stringssystem.starventure.api;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 31.07.13
 * Time: 18:34
 * To change this template use File | Settings | File Templates.
 */
public interface GameObject extends Spawnable {

    long getId();

    boolean isAlive();

    Vector2 getWorldPosition();

    Vector2 getRotationInVector();

    void setAlive(boolean alive);

    void update(float delta);

    Body getBody();

    void setWorldPosition(Vector2 worldPosition);

    void setRotationInDegrees(float degrees);

    Vector2 getBoxPosition();

    void setBoxPosition(Vector2 boxPosition);
}
