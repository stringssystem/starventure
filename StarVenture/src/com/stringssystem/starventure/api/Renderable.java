package com.stringssystem.starventure.api;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 03.09.13
 * Time: 20:30
 * To change this template use File | Settings | File Templates.
 */
public interface Renderable extends Disposable, Comparable<Renderable> {

    Comparator<Renderable> LAYER_COMPARATOR = new Comparator<Renderable>() {
        @Override
        public int compare(Renderable renderable, Renderable renderable2) {
            return renderable.compareTo(renderable2);
        }
    };

    Layer getLayer();

    void setLayer(Layer layer);

    int getWidth();

    int getHeight();

    Vector2 getPosition();

    void setPosition(Vector2 position);

    void setDimension(int width, int height);

    void setColor(Color c);

    void setScale(float x, float y);

    void draw(SpriteBatch sp);

    @Override
    void dispose();

    @Override
    int compareTo(Renderable renderable);

    float getRotation();

    void setRotation(float rotation);
}
