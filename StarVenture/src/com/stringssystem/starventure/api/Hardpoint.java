package com.stringssystem.starventure.api;

import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.ship.Ship;
import com.stringssystem.starventure.ship.component.ComponentType;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 26.09.13
 * Time: 7:59
 * To change this template use File | Settings | File Templates.
 */
public interface Hardpoint<T extends ShipComponent> {
    ComponentType getType();

    void setInitialPosition(Vector2 initialPosition);

    void setShip(Ship ship);

    Ship getShip();

    Vector2 getBoxPosition();

    Vector2 getWorldPosition();

    T getMountedEquipment();

    void setMountedEquipment(T component);
}
