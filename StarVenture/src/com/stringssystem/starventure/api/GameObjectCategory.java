package com.stringssystem.starventure.api;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 01.09.13
 * Time: 15:45
 * To change this template use File | Settings | File Templates.
 */
public enum GameObjectCategory {

    BOUNDARY(0x0001),
    FRIENDLY_SHIP(0x0002),
    ENEMY_SHIP(0x0004),
    FRIENDLY_PROJECTILE(0x0008),
    ENEMY_PROJECTILE(0x0010);

    private int value;

    GameObjectCategory(int value) {
        this.value = value;
    }

    public short mask() {
        return (short) value;
    }
}
