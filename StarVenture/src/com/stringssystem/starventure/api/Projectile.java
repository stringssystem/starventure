package com.stringssystem.starventure.api;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 13.07.13
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
public interface Projectile extends GameObject, ContainsRenderables {

    float getDamage();

    void setDamage(float damage);

    float getBulletSpeed();

    void setBulletSpeed(float bulletSpeed);

    float getRange();

    void setRange(float range);

    void setDrawable(Renderable renderable);
}
