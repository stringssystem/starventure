package com.stringssystem.starventure.api;

import com.badlogic.gdx.physics.box2d.Fixture;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 27.08.13
 * Time: 16:55
 * To change this template use File | Settings | File Templates.
 */
public interface FixtureBindable {

    Fixture getFixture();

    void setFixture(Fixture fixture);
}
