package com.stringssystem.starventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import com.stringssystem.starventure.Player;
import com.stringssystem.starventure.Resources;
import com.stringssystem.starventure.StarVentureGame;
import com.stringssystem.starventure.controls.GameControlsMultiplexer;
import com.stringssystem.starventure.module.*;
import com.stringssystem.starventure.renderer.GameRenderer;
import com.stringssystem.starventure.ship.Ship;
import com.stringssystem.starventure.ship.ShipFactory;
import com.stringssystem.starventure.ship.component.Engine;
import com.stringssystem.starventure.ship.component.TurretFactory;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 08.07.13
 * Time: 16:15
 * To change this template use File | Settings | File Templates.
 */
public final class GameScreen implements Screen {

    private final StarVentureGame game;
    private final GameRenderer gameRenderer = new GameRenderer();

    public GameScreen(StarVentureGame game) {
        this.game = game;
    }

    @Override
    public void render(float v) {
        gameRenderer.render();
    }

    @Override
    public void resize(int i, int i2) {
        gameRenderer.resize(i, i2);
    }

    @Override
    public void show() {
        ModuleManager.registerModules(
                new WorldModule(),
                new CollisionModule(),
                new ParticleEffectsModule(),
                new BackgroundFXModule(),
                new UIModule());

        ShipFactory shipFactory = Resources.getManager().get("data/ships/light_fregate.ship", ShipFactory.class);
        Ship pShip = shipFactory.newInstance();
        pShip.combatSystemsControl.getHardpoints().get(0).setMountedEquipment(Resources.getManager().get("data/weapons/assault_gun.wpn", TurretFactory.class).newInstance());
        pShip.combatSystemsControl.getHardpoints().get(1).setMountedEquipment(Resources.getManager().get("data/weapons/chain_gun.wpn", TurretFactory.class).newInstance());
        Engine engine = new Engine();
        engine.setEffectName("engine-burn2");
        pShip.engineSystemsControl.getHardpoints().get(0).setMountedEquipment(engine);
        engine = new Engine();
        engine.setEffectName("engine-burn2");
        pShip.engineSystemsControl.getHardpoints().get(1).setMountedEquipment(engine);
        pShip.spawn(new Vector2(150, -200), 60);

        Player.getInstance().setShip(pShip);
        shipFactory = Resources.getManager().get("data/ships/capital.ship", ShipFactory.class);
        Ship enemy = shipFactory.newInstance();
        enemy.spawn(new Vector2(500, 100), 230);
        enemy.setEnemy(true);
        enemy = shipFactory.newInstance();
        enemy.spawn(new Vector2(200, 200), 120);
        enemy.setEnemy(true);

        shipFactory = Resources.getManager().get("data/ships/light_fregate.ship", ShipFactory.class);
        enemy = shipFactory.newInstance();
        enemy.spawn(new Vector2(-300, -100), 120);
        enemy.setEnemy(true);
        Gdx.input.setInputProcessor(GameControlsMultiplexer.getInstance().getMultiplexer());
    }

    @Override
    public void hide() {
        gameRenderer.dispose();
        ModuleManager.dispose();
    }

    @Override
    public void pause() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void resume() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void dispose() {
        gameRenderer.dispose();
        ModuleManager.dispose();
    }

}
