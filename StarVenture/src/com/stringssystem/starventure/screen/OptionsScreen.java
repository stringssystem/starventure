package com.stringssystem.starventure.screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.stringssystem.starventure.Constants;
import com.stringssystem.starventure.Settings;
import com.stringssystem.starventure.StarVentureGame;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 30.08.13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */
public class OptionsScreen implements Screen {

    private Stage stage;
    private Texture background;
    private Window window;
    private Window audioWindow;
    private Window graphicsWindow;
    private StarVentureGame game;
    private CheckBox audioCheckBox;
    private CheckBox vsyncCheckBox;
    private CheckBox fullScreenCheckBox;
    private TextButton backButton;
    private TextButton saveButton;
    private Slider fxSlider;
    private Slider musicSlider;
    private List resolutionList;
    private String[] resolutions;
    private Label resolutionLabel;
    private Label musicLabel;
    private Label fxLabel;
    private Label qualityLabel;
    private List qualityList;
    private String[] qualities;

    public OptionsScreen(StarVentureGame game) {
        this.game = game;
    }

    @Override
    public void render(float v) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(v);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.setViewport(width, height, false);
        window.setPosition(width / 2 - window.getWidth() / 2, height / 2 - window.getHeight() / 2);
    }

    private void createComponents() {
        Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        Gdx.input.setInputProcessor(stage);
        background = new Texture(Gdx.files.internal("data/menu_background.jpg"));
        Table t = new Table();
        t.setFillParent(true);
        t.setBackground(new TextureRegionDrawable(new TextureRegion(background)));
        stage.addActor(t);
        audioCheckBox = new CheckBox("Audio enabled", skin);
        audioCheckBox.align(8);
        vsyncCheckBox = new CheckBox("Vsync enabled", skin);
        vsyncCheckBox.align(8);
        fullScreenCheckBox = new CheckBox("Fullscreen", skin);
        fullScreenCheckBox.align(8);
        qualities = new String[]{"low", "medium", "high"};
        qualityLabel = new Label("Quality", skin);
        qualityList = new List(qualities, skin);
        graphicsWindow = new Window("Graphics", skin, "dialog");
        graphicsWindow.setMovable(false);
        graphicsWindow.setModal(false);
        graphicsWindow.defaults().spaceBottom(5);
        saveButton = new TextButton("Save", skin);
        backButton = new TextButton("Back", skin);
        fxSlider = new Slider(0, 100, 1, false, skin);
        fxSlider.setValue(100);
        fxLabel = new Label("Sounds", skin);
        fxLabel.setAlignment(0);
        musicSlider = new Slider(0, 100, 1, false, skin);
        musicSlider.setValue(100);
        musicLabel = new Label("Music", skin);
        musicLabel.setAlignment(0);
        resolutionLabel = new Label("Resolution", skin);
        resolutions = Constants.SUPPORTED_RESOLUTIONS;
        resolutionList = new List(resolutions, skin);
        audioWindow = new Window("Audio", skin, "dialog");
        audioWindow.setMovable(false);
        audioWindow.setModal(false);
        audioWindow.defaults().spaceBottom(5);
        window = new Window("Options", skin, "dialog");
        window.setMovable(false);
        window.setModal(true);
        window.defaults().spaceBottom(10);

        window.row().fill().expandX();
        window.add(audioWindow);
        audioWindow.add(audioCheckBox);
        audioWindow.row().fill().expandX();
        audioWindow.add(musicLabel);
        audioWindow.row().fill().expandX();
        audioWindow.add(musicSlider);
        audioWindow.row().fill().expandX();
        audioWindow.add(fxLabel);
        audioWindow.row().fill().expandX();
        audioWindow.add(fxSlider);
        if (Gdx.app.getType().equals(Application.ApplicationType.Desktop)) {
            window.add(graphicsWindow);
            graphicsWindow.add(vsyncCheckBox);
            graphicsWindow.row().fill().expandX();
            resolutionLabel.setAlignment(0);
            graphicsWindow.add(fullScreenCheckBox);
            graphicsWindow.row().fill().expandX();
            graphicsWindow.add(resolutionLabel);
            graphicsWindow.row().fill().expandX();
            graphicsWindow.add(resolutionList);
            graphicsWindow.row().fill().expandX();
            qualityLabel.setAlignment(0);
            graphicsWindow.add(qualityLabel);
            graphicsWindow.row().fill().expandX();
            graphicsWindow.add(qualityList);
        }

        window.row().fill().expandX();
        window.add(saveButton);

        window.add(backButton);
        window.pack();
        t.addActor(window);

        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                game.setScreen(new MainMenuScreen(game));
            }
        });

        saveButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                saveToSettings();
                game.setScreen(new MainMenuScreen(game));
            }
        });
    }

    private void loadFromSettings() {
        audioCheckBox.setChecked(Settings.getAudioEnabled());
        musicSlider.setValue(Settings.getMusicVolume());
        fxSlider.setValue(Settings.getFxVolume());
        vsyncCheckBox.setChecked(Settings.getVsyncEnabled());
        resolutionList.setSelectedIndex(Settings.getResolutionSelected());
        qualityList.setSelectedIndex(Settings.getQualitySelected());
        fullScreenCheckBox.setChecked(Settings.getFullScreenEnabled());
    }

    private void saveToSettings() {
        Settings.setAudioEnabled(audioCheckBox.isChecked());
        Settings.setMusicVolume((int) musicSlider.getValue());
        Settings.setFxVolume((int) fxSlider.getValue());
        Settings.setVsyncEnabled(vsyncCheckBox.isChecked());
        Settings.setResolutionSelected(resolutionList.getSelectedIndex());
        String s = resolutionList.getSelection();
        String[] resolution = s.split("x");
        Settings.setResolutionSelectedWidth(Integer.parseInt(resolution[0]));
        Settings.setResolutionSelectedHeight(Integer.parseInt(resolution[1]));
        Settings.setQualitySelected(qualityList.getSelectedIndex());
        Settings.setFullscreenEnabled(fullScreenCheckBox.isChecked());
    }

    @Override
    public void show() {
        createComponents();
        loadFromSettings();
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        background.dispose();
        stage.dispose();
    }
}
