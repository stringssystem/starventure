package com.stringssystem.starventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.stringssystem.starventure.StarVentureGame;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 06.08.13
 * Time: 11:01
 * To change this template use File | Settings | File Templates.
 */
public class MainMenuScreen implements Screen {

    private Stage stage;
    private Window window;
    private StarVentureGame game;
    private Texture background;
    private Skin skin;

    public MainMenuScreen(StarVentureGame game) {
        this.game = game;
    }

    @Override
    public void render(float v) {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(v);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.setViewport(width, height, false);
        window.setPosition(width / 2 - window.getWidth() / 2, height / 2 - window.getHeight() / 2);
    }

    @Override
    public void show() {
        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        Gdx.input.setInputProcessor(stage);
        background = new Texture(Gdx.files.internal("data/menu_background.jpg"));
        Table main = new Table();
        main.setFillParent(true);
        main.setBackground(new TextureRegionDrawable(new TextureRegion(background)));
        stage.addActor(main);
        TextButton newGameButton = new TextButton("New Game", skin);
        TextButton optionsButton = new TextButton("Options", skin);
        TextButton quitButton = new TextButton("Quit", skin);
        window = new Window("StarVenture", skin, "dialog");
        window.setMovable(false);
        window.setModal(true);
        window.defaults().spaceBottom(10);
        window.row().fill().expandX();
        window.add(newGameButton);
        window.row().fill().expandX();
        window.add(optionsButton);
        window.row().fill().expandX();
        window.add(quitButton);
        main.addActor(window);

        newGameButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                game.setScreen(new GameScreen(game));
            }
        });

        optionsButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                game.setScreen(new OptionsScreen(game));
            }
        });

        quitButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                Gdx.app.exit();
            }
        });

    }

    @Override
    public void hide() {
        background.dispose();
        skin.dispose();
        stage.dispose();
    }

    @Override
    public void pause() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void resume() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void dispose() {
        background.dispose();
        stage.dispose();
    }
}
