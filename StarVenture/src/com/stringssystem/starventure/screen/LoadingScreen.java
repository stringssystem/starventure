package com.stringssystem.starventure.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.stringssystem.starventure.Resources;
import com.stringssystem.starventure.StarVentureGame;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 24.09.13
 * Time: 21:20
 * To change this template use File | Settings | File Templates.
 */
public class LoadingScreen implements Screen {

    private Stage stage;
    private StarVentureGame game;
    private Skin skin;
    private Slider progress;

    public LoadingScreen(StarVentureGame game) {
        this.game = game;
    }

    @Override
    public void render(float v) {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(v);
        stage.draw();
        if (Resources.getManager().update()) {
            game.setScreen(new MainMenuScreen(game));
        } else {
            progress.setValue(Resources.getManager().getProgress() * 100);
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.setViewport(width, height, false);
    }

    @Override
    public void show() {
        Resources.getInstance().init();
        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
        Gdx.input.setInputProcessor(stage);
        Table main = new Table();
        main.setFillParent(true);
        stage.addActor(main);
        progress = new Slider(0, 100, 1, false, skin, "progress-horizontal");
        progress.setTouchable(Touchable.disabled);
        main.add(new Label("Loading...", skin));
        main.row();
        main.add(progress);
    }

    @Override
    public void hide() {
        skin.dispose();
        stage.dispose();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}
