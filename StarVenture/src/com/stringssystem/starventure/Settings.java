package com.stringssystem.starventure;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Created with IntelliJ IDEA.
 * User: apple
 * Date: 24.09.13
 * Time: 20:14
 * To change this template use File | Settings | File Templates.
 */
public final class Settings {
    private static final Preferences sets = Gdx.app.getPreferences(Constants.SETTINGS_KEY);
    public static final String SOUND_MUSIC = "sound.music";
    public static final String SOUND_FX = "sound.fx";
    public static final String SOUND_ENABLED = "sound.enabled";
    public static final String VSYNC_ENABLED = "graphics.vsync.enabled";
    public static final String RESOLUTION_SELECTED_WIDTH = "graphics.resolution.width";
    public static final String RESOLUTION_SELECTED_HEIGHT = "graphics.resolutin.height";
    public static final String RESOLUTION_SELECTED = "graphics.resolution.selected";
    public static final String QUALITY_SELECTED = "graphics.quality.selected";
    public static final String FULLSCREEN_ENABLED = "graphics.fullscreen.enabled";


    public static int getMusicVolume() {
        return sets.getInteger(SOUND_MUSIC, 100);
    }

    public static void setMusicVolume(int soundVolume) {
        sets.putInteger(SOUND_MUSIC, soundVolume);
        sets.flush();
    }

    public static int getFxVolume() {
        return sets.getInteger(SOUND_FX, 100);
    }

    public static void setFxVolume(int fxVolume) {
        sets.putInteger(SOUND_FX, fxVolume);
        sets.flush();
    }

    public static boolean getAudioEnabled() {
        return sets.getBoolean(SOUND_ENABLED, true);
    }

    public static void setAudioEnabled(boolean enabled) {
        sets.putBoolean(SOUND_ENABLED, enabled);
        sets.flush();
    }

    public static boolean getVsyncEnabled() {
        return sets.getBoolean(VSYNC_ENABLED, true);
    }

    public static void setVsyncEnabled(boolean enabled) {
        sets.putBoolean(VSYNC_ENABLED, enabled);
        sets.flush();
    }

    public static int getResolutionSelectedWidth() {
        return sets.getInteger(RESOLUTION_SELECTED_WIDTH, 800);
    }

    public static void setResolutionSelectedWidth(int width) {
        sets.putInteger(RESOLUTION_SELECTED_WIDTH, width);
        sets.flush();
    }

    public static int getResolutionSelectedHeight() {
        return sets.getInteger(RESOLUTION_SELECTED_HEIGHT, 600);
    }

    public static void setResolutionSelectedHeight(int height) {
        sets.putInteger(RESOLUTION_SELECTED_HEIGHT, height);
        sets.flush();
    }

    public static int getResolutionSelected() {
        return sets.getInteger(RESOLUTION_SELECTED, 0);
    }

    public static void setResolutionSelected(int index) {
        sets.putInteger(RESOLUTION_SELECTED, index);
        sets.flush();
    }

    public static int getQualitySelected() {
        return sets.getInteger(QUALITY_SELECTED);
    }

    public static void setQualitySelected(int quality) {
        sets.putInteger(QUALITY_SELECTED, quality);
        sets.flush();
    }

    public static boolean getFullScreenEnabled() {
        return sets.getBoolean(FULLSCREEN_ENABLED, false);
    }

    public static void setFullscreenEnabled(boolean enabled) {
        sets.putBoolean(FULLSCREEN_ENABLED, enabled);
        sets.flush();
    }
}
