package com.stringssystem.starventure;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

public class Main {
    public static void main(String[] args) {
        //packSprites();
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "StarVenture";
        cfg.useGL20 = true;
        cfg.fullscreen = false;
        new LwjglApplication(new StarVentureGame(), cfg);

    }

    private static void packSprites() {
        TexturePacker2.Settings settings = new TexturePacker2.Settings();
        settings.filterMin = Texture.TextureFilter.MipMapLinearNearest;
        settings.filterMag = Texture.TextureFilter.Nearest;
        TexturePacker2.processIfModified(settings, "data/sprites/", "data/spritepack/", "pack");
    }
}
